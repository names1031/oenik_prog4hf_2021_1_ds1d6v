var namespace_feleves_feladat_1_1_logic =
[
    [ "Tests", "namespace_feleves_feladat_1_1_logic_1_1_tests.html", "namespace_feleves_feladat_1_1_logic_1_1_tests" ],
    [ "BandsOriginAlbumLength", "class_feleves_feladat_1_1_logic_1_1_bands_origin_album_length.html", "class_feleves_feladat_1_1_logic_1_1_bands_origin_album_length" ],
    [ "BillboardAvargaeResult", "class_feleves_feladat_1_1_logic_1_1_billboard_avargae_result.html", "class_feleves_feladat_1_1_logic_1_1_billboard_avargae_result" ],
    [ "EditDataStackLogic", "class_feleves_feladat_1_1_logic_1_1_edit_data_stack_logic.html", "class_feleves_feladat_1_1_logic_1_1_edit_data_stack_logic" ],
    [ "GetEntityByDefinitionLogic", "class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html", "class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic" ],
    [ "IEditDataStackLogic", "interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic.html", "interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic" ],
    [ "IGetEntityByDefinitionLogic", "interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html", "interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic" ],
    [ "IModifyEntityLogic", "interface_feleves_feladat_1_1_logic_1_1_i_modify_entity_logic.html", "interface_feleves_feladat_1_1_logic_1_1_i_modify_entity_logic" ],
    [ "ModifyEntityLogic", "class_feleves_feladat_1_1_logic_1_1_modify_entity_logic.html", "class_feleves_feladat_1_1_logic_1_1_modify_entity_logic" ],
    [ "MostCommonMonth", "class_feleves_feladat_1_1_logic_1_1_most_common_month.html", "class_feleves_feladat_1_1_logic_1_1_most_common_month" ],
    [ "SongsWithVideos", "class_feleves_feladat_1_1_logic_1_1_songs_with_videos.html", "class_feleves_feladat_1_1_logic_1_1_songs_with_videos" ]
];