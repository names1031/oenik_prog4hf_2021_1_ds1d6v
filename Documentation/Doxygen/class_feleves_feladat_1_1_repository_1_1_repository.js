var class_feleves_feladat_1_1_repository_1_1_repository =
[
    [ "Repository", "class_feleves_feladat_1_1_repository_1_1_repository.html#ab057885d3f0e7a41dddbd3aff5fd8446", null ],
    [ "GetAll", "class_feleves_feladat_1_1_repository_1_1_repository.html#a6448c052af1076a49d832900eae65dd1", null ],
    [ "GetOne", "class_feleves_feladat_1_1_repository_1_1_repository.html#aaca3c7a76e15011b153246aae4d61d75", null ],
    [ "Insert", "class_feleves_feladat_1_1_repository_1_1_repository.html#a485a1023a7e525514ab165ba7fb0658b", null ],
    [ "Remove", "class_feleves_feladat_1_1_repository_1_1_repository.html#af7e04db0b5c47d01d178ed8e30024395", null ],
    [ "Ctx", "class_feleves_feladat_1_1_repository_1_1_repository.html#a99c431902b81ddd2649ebe2b063efdaa", null ]
];