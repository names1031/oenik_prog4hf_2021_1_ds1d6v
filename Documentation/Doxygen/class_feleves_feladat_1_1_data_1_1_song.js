var class_feleves_feladat_1_1_data_1_1_song =
[
    [ "Equals", "class_feleves_feladat_1_1_data_1_1_song.html#adbf15518b72bd150465e20972b78ad0f", null ],
    [ "GetHashCode", "class_feleves_feladat_1_1_data_1_1_song.html#a1879ca2901fd6a4e0ff0e5ca581c61f0", null ],
    [ "Album", "class_feleves_feladat_1_1_data_1_1_song.html#a85a89e8409e564631f3294f2ac648f4f", null ],
    [ "AlbumID", "class_feleves_feladat_1_1_data_1_1_song.html#a8019aec6360779615c0825271cef5b06", null ],
    [ "Billboard", "class_feleves_feladat_1_1_data_1_1_song.html#ab29f81f7a5796b3dd8807bd0148bb4f4", null ],
    [ "GuitarSolo", "class_feleves_feladat_1_1_data_1_1_song.html#a077ddddacba806a89f2a3dc9d3749829", null ],
    [ "ID", "class_feleves_feladat_1_1_data_1_1_song.html#a7e1a9efdf3dc6b7fec0696838cb8f13e", null ],
    [ "Length", "class_feleves_feladat_1_1_data_1_1_song.html#a3d52d3e60e5952dc77ead4958709273f", null ],
    [ "MusicVideo", "class_feleves_feladat_1_1_data_1_1_song.html#a39bb951a6c1e04048180b52572e4b605", null ],
    [ "Name", "class_feleves_feladat_1_1_data_1_1_song.html#ad7cad9e36b003b28e2f71c16e17e8df2", null ]
];