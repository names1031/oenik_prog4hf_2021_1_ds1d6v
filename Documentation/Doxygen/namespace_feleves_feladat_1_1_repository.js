var namespace_feleves_feladat_1_1_repository =
[
    [ "AlbumRepository", "class_feleves_feladat_1_1_repository_1_1_album_repository.html", "class_feleves_feladat_1_1_repository_1_1_album_repository" ],
    [ "BandRepository", "class_feleves_feladat_1_1_repository_1_1_band_repository.html", "class_feleves_feladat_1_1_repository_1_1_band_repository" ],
    [ "IAlbumRepository", "interface_feleves_feladat_1_1_repository_1_1_i_album_repository.html", "interface_feleves_feladat_1_1_repository_1_1_i_album_repository" ],
    [ "IBandRepository", "interface_feleves_feladat_1_1_repository_1_1_i_band_repository.html", "interface_feleves_feladat_1_1_repository_1_1_i_band_repository" ],
    [ "IRepository", "interface_feleves_feladat_1_1_repository_1_1_i_repository.html", "interface_feleves_feladat_1_1_repository_1_1_i_repository" ],
    [ "ISongRepository", "interface_feleves_feladat_1_1_repository_1_1_i_song_repository.html", "interface_feleves_feladat_1_1_repository_1_1_i_song_repository" ],
    [ "Repository", "class_feleves_feladat_1_1_repository_1_1_repository.html", "class_feleves_feladat_1_1_repository_1_1_repository" ],
    [ "SongRepository", "class_feleves_feladat_1_1_repository_1_1_song_repository.html", "class_feleves_feladat_1_1_repository_1_1_song_repository" ]
];