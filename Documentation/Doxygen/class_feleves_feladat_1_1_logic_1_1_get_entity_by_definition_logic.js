var class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic =
[
    [ "GetEntityByDefinitionLogic", "class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html#ab9d54b09a81d7f4cc601a286c6a8c12e", null ],
    [ "AlbumsByMonthsCount", "class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html#a9bdff2747d1fe2403d272f89751bb682", null ],
    [ "AlbumsSongBillboardAvaragePlaceAsync", "class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html#a2b7b1aa8471ec7596f7e896332dfcbaa", null ],
    [ "AlbumsSongsBillboardAvaragePlace", "class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html#ada7c07d1d085f4ec5f36ead2b8e5e7cb", null ],
    [ "BandsOriginPlaces", "class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html#a46a7f84593dd38cfe20551266a9234bd", null ],
    [ "BandsOriginPlacesAsync", "class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html#a146f37c7d1490dbefaf730c75286ec86", null ],
    [ "ContainsMusicVideo", "class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html#ab383d98caa3b1881df99e3cb5176eabb", null ],
    [ "ContainsMusicVideoAsync", "class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html#a0007a2e9a64ebfad2b93ca7e3608b98e", null ],
    [ "GetAlbumById", "class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html#a52c126be62fcf4088c2f3bdec615e38c", null ],
    [ "GetAllAlbums", "class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html#ab1fbb1ed51f6fd5dbc5daefd2277a74e", null ],
    [ "GetAllBands", "class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html#aacf272591db5a633025060a0aec529fa", null ],
    [ "GetAllSongs", "class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html#ac3cf76c55459ed0540969df06c29ea85", null ],
    [ "GetBandById", "class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html#adfb0f40ef118445e2bc8f8a01635352f", null ],
    [ "GetSongById", "class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html#a6b6674780d2ec7d61b0c8d257514a9f3", null ]
];