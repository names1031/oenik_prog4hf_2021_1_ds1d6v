var class_feleves_feladat_1_1_logic_1_1_edit_data_stack_logic =
[
    [ "EditDataStackLogic", "class_feleves_feladat_1_1_logic_1_1_edit_data_stack_logic.html#a84f3afa12d7bac34b5b8b358c304eff2", null ],
    [ "InsertAlbum", "class_feleves_feladat_1_1_logic_1_1_edit_data_stack_logic.html#a0047af09a44a0c219567e1b21ec116e7", null ],
    [ "InsertBand", "class_feleves_feladat_1_1_logic_1_1_edit_data_stack_logic.html#ac6f17377a6e10bfec89a4b6e2368e10c", null ],
    [ "InsertSong", "class_feleves_feladat_1_1_logic_1_1_edit_data_stack_logic.html#a4f53bfbccdfae57da517f2fd4986408c", null ],
    [ "RemoveAlbum", "class_feleves_feladat_1_1_logic_1_1_edit_data_stack_logic.html#a6252ac407aee2ecf0d30e1444637cdda", null ],
    [ "RemoveBand", "class_feleves_feladat_1_1_logic_1_1_edit_data_stack_logic.html#a3df0ca9ebf5874b52f7c639c49517c96", null ],
    [ "RemoveSong", "class_feleves_feladat_1_1_logic_1_1_edit_data_stack_logic.html#ad399052df6eb18a7660cf8f66e472300", null ]
];