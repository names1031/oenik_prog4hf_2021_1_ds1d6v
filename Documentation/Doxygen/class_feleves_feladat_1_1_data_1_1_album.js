var class_feleves_feladat_1_1_data_1_1_album =
[
    [ "Album", "class_feleves_feladat_1_1_data_1_1_album.html#ac2e528daac924e87b6f30f94d56e673e", null ],
    [ "Equals", "class_feleves_feladat_1_1_data_1_1_album.html#a96febf2fb4020b91f51149813c94ac79", null ],
    [ "GetHashCode", "class_feleves_feladat_1_1_data_1_1_album.html#a43a422e4953b8f4e212866b8facdb59c", null ],
    [ "Band", "class_feleves_feladat_1_1_data_1_1_album.html#a3f9fcf089d4ad15c6356836679905037", null ],
    [ "BandID", "class_feleves_feladat_1_1_data_1_1_album.html#a464628fed2d6c446281a5e384f37cb93", null ],
    [ "ID", "class_feleves_feladat_1_1_data_1_1_album.html#a36902dae5a2b1193f7e5d1d1f83016ec", null ],
    [ "Label", "class_feleves_feladat_1_1_data_1_1_album.html#a51d3a4a1abac233d5dd4604b16564065", null ],
    [ "Language", "class_feleves_feladat_1_1_data_1_1_album.html#a3555224d354e2fc91ae3831302c05db4", null ],
    [ "Length", "class_feleves_feladat_1_1_data_1_1_album.html#a51440bfdaf4f23edfe9dbddf1c747ab3", null ],
    [ "Released", "class_feleves_feladat_1_1_data_1_1_album.html#ad07667334bf050ab04e1212f69939a68", null ],
    [ "Songs", "class_feleves_feladat_1_1_data_1_1_album.html#ac4c81726c5bad9cf704ed6a4e096f033", null ],
    [ "Title", "class_feleves_feladat_1_1_data_1_1_album.html#aa7b07961ea4ce0d22ba96fc8ad366b75", null ]
];