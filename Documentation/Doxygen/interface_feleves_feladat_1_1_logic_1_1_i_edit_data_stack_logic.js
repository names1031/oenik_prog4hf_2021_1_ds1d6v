var interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic =
[
    [ "InsertAlbum", "interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic.html#a6967a091c14a7dd9e26cdc787393debd", null ],
    [ "InsertBand", "interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic.html#ae7156478070ad24f7c7485719c47aa9f", null ],
    [ "InsertSong", "interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic.html#a84c0539fa42af5a50dd5558de51b2730", null ],
    [ "RemoveAlbum", "interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic.html#ae81805438d25653aed4264d649aaea84", null ],
    [ "RemoveBand", "interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic.html#aa37c66968d61de04c8c50c4d804164a9", null ],
    [ "RemoveSong", "interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic.html#a68ededaaac7e3610577bfafa0da4f16f", null ]
];