var class_feleves_feladat_1_1_data_1_1_band =
[
    [ "Band", "class_feleves_feladat_1_1_data_1_1_band.html#a616a23169654f81270c391a409646198", null ],
    [ "Equals", "class_feleves_feladat_1_1_data_1_1_band.html#a57fa5d9b5e2a5f59b0299af857f1f3f8", null ],
    [ "GetHashCode", "class_feleves_feladat_1_1_data_1_1_band.html#a76c966b393ae267c83c395a4107887ae", null ],
    [ "Albums", "class_feleves_feladat_1_1_data_1_1_band.html#aa1e2ef8223b315ec772c7db12caf2d62", null ],
    [ "Formed", "class_feleves_feladat_1_1_data_1_1_band.html#ad55d1fb73a8d9fcef346a6150b567dbe", null ],
    [ "Genres", "class_feleves_feladat_1_1_data_1_1_band.html#ae0c170d5dea11a1395c135974b3008b3", null ],
    [ "ID", "class_feleves_feladat_1_1_data_1_1_band.html#a541155ff8f91fa510da5d9a008499caa", null ],
    [ "Members", "class_feleves_feladat_1_1_data_1_1_band.html#a0435d234729a26c51074f58b3dc1a16c", null ],
    [ "Name", "class_feleves_feladat_1_1_data_1_1_band.html#abe933c05c26c0b800ed8dac7b063fedb", null ],
    [ "Origin", "class_feleves_feladat_1_1_data_1_1_band.html#a00dfbee2314ec4c6bb7b383f3c9d0bf5", null ]
];