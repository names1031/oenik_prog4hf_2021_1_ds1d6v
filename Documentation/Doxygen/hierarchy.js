var hierarchy =
[
    [ "FelevesFeladat.Data.Album", "class_feleves_feladat_1_1_data_1_1_album.html", null ],
    [ "FelevesFeladat.Data.Band", "class_feleves_feladat_1_1_data_1_1_band.html", null ],
    [ "FelevesFeladat.Logic.BandsOriginAlbumLength", "class_feleves_feladat_1_1_logic_1_1_bands_origin_album_length.html", null ],
    [ "FelevesFeladat.Logic.BillboardAvargaeResult", "class_feleves_feladat_1_1_logic_1_1_billboard_avargae_result.html", null ],
    [ "DbContext", null, [
      [ "FelevesFeladat.Data.BandContext", "class_feleves_feladat_1_1_data_1_1_band_context.html", null ]
    ] ],
    [ "FelevesFeladat.Logic.Tests.EditDataStackTests", "class_feleves_feladat_1_1_logic_1_1_tests_1_1_edit_data_stack_tests.html", null ],
    [ "FelevesFeladat.Program.Factory", "class_feleves_feladat_1_1_program_1_1_factory.html", null ],
    [ "FelevesFeladat.Logic.Tests.GetEntityByDefinitionTests", "class_feleves_feladat_1_1_logic_1_1_tests_1_1_get_entity_by_definition_tests.html", null ],
    [ "FelevesFeladat.Logic.IEditDataStackLogic", "interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic.html", [
      [ "FelevesFeladat.Logic.EditDataStackLogic", "class_feleves_feladat_1_1_logic_1_1_edit_data_stack_logic.html", null ]
    ] ],
    [ "FelevesFeladat.Logic.IGetEntityByDefinitionLogic", "interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html", [
      [ "FelevesFeladat.Logic.GetEntityByDefinitionLogic", "class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html", null ]
    ] ],
    [ "FelevesFeladat.Logic.IModifyEntityLogic", "interface_feleves_feladat_1_1_logic_1_1_i_modify_entity_logic.html", [
      [ "FelevesFeladat.Logic.ModifyEntityLogic", "class_feleves_feladat_1_1_logic_1_1_modify_entity_logic.html", null ]
    ] ],
    [ "FelevesFeladat.Repository.IRepository< T >", "interface_feleves_feladat_1_1_repository_1_1_i_repository.html", [
      [ "FelevesFeladat.Repository.Repository< T >", "class_feleves_feladat_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "FelevesFeladat.Repository.IRepository< Album >", "interface_feleves_feladat_1_1_repository_1_1_i_repository.html", [
      [ "FelevesFeladat.Repository.IAlbumRepository", "interface_feleves_feladat_1_1_repository_1_1_i_album_repository.html", [
        [ "FelevesFeladat.Repository.AlbumRepository", "class_feleves_feladat_1_1_repository_1_1_album_repository.html", null ]
      ] ]
    ] ],
    [ "FelevesFeladat.Repository.IRepository< Band >", "interface_feleves_feladat_1_1_repository_1_1_i_repository.html", [
      [ "FelevesFeladat.Repository.IBandRepository", "interface_feleves_feladat_1_1_repository_1_1_i_band_repository.html", [
        [ "FelevesFeladat.Repository.BandRepository", "class_feleves_feladat_1_1_repository_1_1_band_repository.html", null ]
      ] ]
    ] ],
    [ "FelevesFeladat.Repository.IRepository< Song >", "interface_feleves_feladat_1_1_repository_1_1_i_repository.html", [
      [ "FelevesFeladat.Repository.ISongRepository", "interface_feleves_feladat_1_1_repository_1_1_i_song_repository.html", [
        [ "FelevesFeladat.Repository.SongRepository", "class_feleves_feladat_1_1_repository_1_1_song_repository.html", null ]
      ] ]
    ] ],
    [ "FelevesFeladat.Logic.Tests.ModifyEntityLogicTests", "class_feleves_feladat_1_1_logic_1_1_tests_1_1_modify_entity_logic_tests.html", null ],
    [ "FelevesFeladat.Logic.MostCommonMonth", "class_feleves_feladat_1_1_logic_1_1_most_common_month.html", null ],
    [ "FelevesFeladat.Program.Program", "class_feleves_feladat_1_1_program_1_1_program.html", null ],
    [ "FelevesFeladat.Repository.Repository< Album >", "class_feleves_feladat_1_1_repository_1_1_repository.html", [
      [ "FelevesFeladat.Repository.AlbumRepository", "class_feleves_feladat_1_1_repository_1_1_album_repository.html", null ]
    ] ],
    [ "FelevesFeladat.Repository.Repository< Band >", "class_feleves_feladat_1_1_repository_1_1_repository.html", [
      [ "FelevesFeladat.Repository.BandRepository", "class_feleves_feladat_1_1_repository_1_1_band_repository.html", null ]
    ] ],
    [ "FelevesFeladat.Repository.Repository< Song >", "class_feleves_feladat_1_1_repository_1_1_repository.html", [
      [ "FelevesFeladat.Repository.SongRepository", "class_feleves_feladat_1_1_repository_1_1_song_repository.html", null ]
    ] ],
    [ "FelevesFeladat.Data.Song", "class_feleves_feladat_1_1_data_1_1_song.html", null ],
    [ "FelevesFeladat.Logic.SongsWithVideos", "class_feleves_feladat_1_1_logic_1_1_songs_with_videos.html", null ]
];