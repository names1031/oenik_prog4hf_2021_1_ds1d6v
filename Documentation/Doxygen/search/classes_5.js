var searchData=
[
  ['ialbumrepository_135',['IAlbumRepository',['../interface_feleves_feladat_1_1_repository_1_1_i_album_repository.html',1,'FelevesFeladat::Repository']]],
  ['ibandrepository_136',['IBandRepository',['../interface_feleves_feladat_1_1_repository_1_1_i_band_repository.html',1,'FelevesFeladat::Repository']]],
  ['ieditdatastacklogic_137',['IEditDataStackLogic',['../interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic.html',1,'FelevesFeladat::Logic']]],
  ['igetentitybydefinitionlogic_138',['IGetEntityByDefinitionLogic',['../interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html',1,'FelevesFeladat::Logic']]],
  ['imodifyentitylogic_139',['IModifyEntityLogic',['../interface_feleves_feladat_1_1_logic_1_1_i_modify_entity_logic.html',1,'FelevesFeladat::Logic']]],
  ['irepository_140',['IRepository',['../interface_feleves_feladat_1_1_repository_1_1_i_repository.html',1,'FelevesFeladat::Repository']]],
  ['irepository_3c_20album_20_3e_141',['IRepository&lt; Album &gt;',['../interface_feleves_feladat_1_1_repository_1_1_i_repository.html',1,'FelevesFeladat::Repository']]],
  ['irepository_3c_20band_20_3e_142',['IRepository&lt; Band &gt;',['../interface_feleves_feladat_1_1_repository_1_1_i_repository.html',1,'FelevesFeladat::Repository']]],
  ['irepository_3c_20song_20_3e_143',['IRepository&lt; Song &gt;',['../interface_feleves_feladat_1_1_repository_1_1_i_repository.html',1,'FelevesFeladat::Repository']]],
  ['isongrepository_144',['ISongRepository',['../interface_feleves_feladat_1_1_repository_1_1_i_song_repository.html',1,'FelevesFeladat::Repository']]]
];
