var searchData=
[
  ['ialbumrepository_59',['IAlbumRepository',['../interface_feleves_feladat_1_1_repository_1_1_i_album_repository.html',1,'FelevesFeladat::Repository']]],
  ['ibandrepository_60',['IBandRepository',['../interface_feleves_feladat_1_1_repository_1_1_i_band_repository.html',1,'FelevesFeladat::Repository']]],
  ['id_61',['ID',['../class_feleves_feladat_1_1_data_1_1_album.html#a36902dae5a2b1193f7e5d1d1f83016ec',1,'FelevesFeladat.Data.Album.ID()'],['../class_feleves_feladat_1_1_data_1_1_band.html#a541155ff8f91fa510da5d9a008499caa',1,'FelevesFeladat.Data.Band.ID()'],['../class_feleves_feladat_1_1_data_1_1_song.html#a7e1a9efdf3dc6b7fec0696838cb8f13e',1,'FelevesFeladat.Data.Song.ID()']]],
  ['ieditdatastacklogic_62',['IEditDataStackLogic',['../interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic.html',1,'FelevesFeladat::Logic']]],
  ['igetentitybydefinitionlogic_63',['IGetEntityByDefinitionLogic',['../interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html',1,'FelevesFeladat::Logic']]],
  ['imodifyentitylogic_64',['IModifyEntityLogic',['../interface_feleves_feladat_1_1_logic_1_1_i_modify_entity_logic.html',1,'FelevesFeladat::Logic']]],
  ['initialization_65',['Initialization',['../class_feleves_feladat_1_1_program_1_1_factory.html#a604d78b8444078cf3f08abd31ec3b96e',1,'FelevesFeladat::Program::Factory']]],
  ['insert_66',['Insert',['../interface_feleves_feladat_1_1_repository_1_1_i_repository.html#a9078fc1587790d25b17e9f69bba3b293',1,'FelevesFeladat.Repository.IRepository.Insert()'],['../class_feleves_feladat_1_1_repository_1_1_repository.html#a485a1023a7e525514ab165ba7fb0658b',1,'FelevesFeladat.Repository.Repository.Insert()']]],
  ['insertalbum_67',['InsertAlbum',['../class_feleves_feladat_1_1_logic_1_1_edit_data_stack_logic.html#a0047af09a44a0c219567e1b21ec116e7',1,'FelevesFeladat.Logic.EditDataStackLogic.InsertAlbum()'],['../interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic.html#a6967a091c14a7dd9e26cdc787393debd',1,'FelevesFeladat.Logic.IEditDataStackLogic.InsertAlbum()']]],
  ['insertband_68',['InsertBand',['../class_feleves_feladat_1_1_logic_1_1_edit_data_stack_logic.html#ac6f17377a6e10bfec89a4b6e2368e10c',1,'FelevesFeladat.Logic.EditDataStackLogic.InsertBand()'],['../interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic.html#ae7156478070ad24f7c7485719c47aa9f',1,'FelevesFeladat.Logic.IEditDataStackLogic.InsertBand()']]],
  ['insertsong_69',['InsertSong',['../class_feleves_feladat_1_1_logic_1_1_edit_data_stack_logic.html#a4f53bfbccdfae57da517f2fd4986408c',1,'FelevesFeladat.Logic.EditDataStackLogic.InsertSong()'],['../interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic.html#a84c0539fa42af5a50dd5558de51b2730',1,'FelevesFeladat.Logic.IEditDataStackLogic.InsertSong()']]],
  ['irepository_70',['IRepository',['../interface_feleves_feladat_1_1_repository_1_1_i_repository.html',1,'FelevesFeladat::Repository']]],
  ['irepository_3c_20album_20_3e_71',['IRepository&lt; Album &gt;',['../interface_feleves_feladat_1_1_repository_1_1_i_repository.html',1,'FelevesFeladat::Repository']]],
  ['irepository_3c_20band_20_3e_72',['IRepository&lt; Band &gt;',['../interface_feleves_feladat_1_1_repository_1_1_i_repository.html',1,'FelevesFeladat::Repository']]],
  ['irepository_3c_20song_20_3e_73',['IRepository&lt; Song &gt;',['../interface_feleves_feladat_1_1_repository_1_1_i_repository.html',1,'FelevesFeladat::Repository']]],
  ['isongrepository_74',['ISongRepository',['../interface_feleves_feladat_1_1_repository_1_1_i_song_repository.html',1,'FelevesFeladat::Repository']]]
];
