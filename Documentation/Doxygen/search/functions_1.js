var searchData=
[
  ['band_168',['Band',['../class_feleves_feladat_1_1_data_1_1_band.html#a616a23169654f81270c391a409646198',1,'FelevesFeladat::Data::Band']]],
  ['bandchangename_169',['BandChangeName',['../class_feleves_feladat_1_1_repository_1_1_band_repository.html#ae1332cf015e6c83d24344a4216d59083',1,'FelevesFeladat.Repository.BandRepository.BandChangeName()'],['../interface_feleves_feladat_1_1_repository_1_1_i_band_repository.html#a3ecee1c40aa0c58c6f5db195d46edf6e',1,'FelevesFeladat.Repository.IBandRepository.BandChangeName()']]],
  ['bandcontext_170',['BandContext',['../class_feleves_feladat_1_1_data_1_1_band_context.html#a5b7caf7e541a05e6e977fdc3194588c2',1,'FelevesFeladat::Data::BandContext']]],
  ['bandrepository_171',['BandRepository',['../class_feleves_feladat_1_1_repository_1_1_band_repository.html#a0673c01fd68ccef2ac38e6f3108c620a',1,'FelevesFeladat::Repository::BandRepository']]],
  ['bandsoriginplaces_172',['BandsOriginPlaces',['../class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html#a46a7f84593dd38cfe20551266a9234bd',1,'FelevesFeladat.Logic.GetEntityByDefinitionLogic.BandsOriginPlaces()'],['../interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html#a324ed65b2347f3689f8b31e24e6f6465',1,'FelevesFeladat.Logic.IGetEntityByDefinitionLogic.BandsOriginPlaces()']]],
  ['bandsoriginplacesasync_173',['BandsOriginPlacesAsync',['../class_feleves_feladat_1_1_logic_1_1_get_entity_by_definition_logic.html#a146f37c7d1490dbefaf730c75286ec86',1,'FelevesFeladat.Logic.GetEntityByDefinitionLogic.BandsOriginPlacesAsync()'],['../interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html#adf78d56a7639c0b9a52c8b7ccef3eafb',1,'FelevesFeladat.Logic.IGetEntityByDefinitionLogic.BandsOriginPlacesAsync()']]]
];
