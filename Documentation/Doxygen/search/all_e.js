var searchData=
[
  ['released_97',['Released',['../class_feleves_feladat_1_1_data_1_1_album.html#ad07667334bf050ab04e1212f69939a68',1,'FelevesFeladat::Data::Album']]],
  ['remove_98',['Remove',['../interface_feleves_feladat_1_1_repository_1_1_i_repository.html#ae292e7a5c12f6d460b7533efa777964a',1,'FelevesFeladat.Repository.IRepository.Remove()'],['../class_feleves_feladat_1_1_repository_1_1_repository.html#af7e04db0b5c47d01d178ed8e30024395',1,'FelevesFeladat.Repository.Repository.Remove()']]],
  ['removealbum_99',['RemoveAlbum',['../class_feleves_feladat_1_1_logic_1_1_edit_data_stack_logic.html#a6252ac407aee2ecf0d30e1444637cdda',1,'FelevesFeladat.Logic.EditDataStackLogic.RemoveAlbum()'],['../interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic.html#ae81805438d25653aed4264d649aaea84',1,'FelevesFeladat.Logic.IEditDataStackLogic.RemoveAlbum()']]],
  ['removeband_100',['RemoveBand',['../class_feleves_feladat_1_1_logic_1_1_edit_data_stack_logic.html#a3df0ca9ebf5874b52f7c639c49517c96',1,'FelevesFeladat.Logic.EditDataStackLogic.RemoveBand()'],['../interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic.html#aa37c66968d61de04c8c50c4d804164a9',1,'FelevesFeladat.Logic.IEditDataStackLogic.RemoveBand()']]],
  ['removesong_101',['RemoveSong',['../class_feleves_feladat_1_1_logic_1_1_edit_data_stack_logic.html#ad399052df6eb18a7660cf8f66e472300',1,'FelevesFeladat.Logic.EditDataStackLogic.RemoveSong()'],['../interface_feleves_feladat_1_1_logic_1_1_i_edit_data_stack_logic.html#a68ededaaac7e3610577bfafa0da4f16f',1,'FelevesFeladat.Logic.IEditDataStackLogic.RemoveSong()']]],
  ['repository_102',['Repository',['../class_feleves_feladat_1_1_repository_1_1_repository.html',1,'FelevesFeladat.Repository.Repository&lt; T &gt;'],['../class_feleves_feladat_1_1_repository_1_1_repository.html#ab057885d3f0e7a41dddbd3aff5fd8446',1,'FelevesFeladat.Repository.Repository.Repository()']]],
  ['repository_3c_20album_20_3e_103',['Repository&lt; Album &gt;',['../class_feleves_feladat_1_1_repository_1_1_repository.html',1,'FelevesFeladat::Repository']]],
  ['repository_3c_20band_20_3e_104',['Repository&lt; Band &gt;',['../class_feleves_feladat_1_1_repository_1_1_repository.html',1,'FelevesFeladat::Repository']]],
  ['repository_3c_20song_20_3e_105',['Repository&lt; Song &gt;',['../class_feleves_feladat_1_1_repository_1_1_repository.html',1,'FelevesFeladat::Repository']]]
];
