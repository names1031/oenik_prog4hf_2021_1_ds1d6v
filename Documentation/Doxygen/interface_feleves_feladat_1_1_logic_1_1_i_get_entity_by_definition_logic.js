var interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic =
[
    [ "AlbumsByMonthsCount", "interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html#af1b177a9328cb9e4f898a24ffc7289a1", null ],
    [ "AlbumsSongBillboardAvaragePlaceAsync", "interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html#a771e3280bb6130b54cb96cff513f5952", null ],
    [ "AlbumsSongsBillboardAvaragePlace", "interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html#ac428bc5d84978ada9b4b6e64d3a57f2b", null ],
    [ "BandsOriginPlaces", "interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html#a324ed65b2347f3689f8b31e24e6f6465", null ],
    [ "BandsOriginPlacesAsync", "interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html#adf78d56a7639c0b9a52c8b7ccef3eafb", null ],
    [ "ContainsMusicVideo", "interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html#a44cc51868347e98de7a620319ac17d11", null ],
    [ "ContainsMusicVideoAsync", "interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html#a9d66c857dee3c2015d9c720dcaa80d50", null ],
    [ "GetAlbumById", "interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html#a705269191489901a394df0ffa0a5a7de", null ],
    [ "GetAllAlbums", "interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html#a620f6a8bbce8f721b2b059087f35e399", null ],
    [ "GetAllBands", "interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html#a1951d8cbd34f5356ba73f5484d43d33d", null ],
    [ "GetAllSongs", "interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html#a34bf9c69b0d413cefba4fca724fdbeb0", null ],
    [ "GetBandById", "interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html#a0e7dd0458abf034442aed49f8e588a25", null ],
    [ "GetSongById", "interface_feleves_feladat_1_1_logic_1_1_i_get_entity_by_definition_logic.html#a4e9d041df015a5b85140da3180e208da", null ]
];