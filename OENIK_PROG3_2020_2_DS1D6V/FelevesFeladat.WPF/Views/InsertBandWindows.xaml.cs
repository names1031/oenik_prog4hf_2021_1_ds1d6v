﻿// <copyright file="InsertBandWindows.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WPF
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for InsertBandWindows.xaml.
    /// </summary>
    public partial class InsertBandWindows : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InsertBandWindows"/> class.
        /// The consturctor of the window.
        /// </summary>
        public InsertBandWindows()
        {
            this.InitializeComponent();

            this.VM = this.DataContext as BandViewModel;
        }

        /// <summary>
        /// Gets or sets the window's viewmodel.
        /// </summary>
        public BandViewModel VM { get; set; }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = new BandViewModel();
        }

        /// <summary>
        /// The event that occures when the inserting is accepted by a button click.
        /// </summary>
        /// <param name="sender">The object that sends the evenet.</param>
        /// <param name="e">Contains state information and event data.</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
