﻿// <copyright file="SongViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WPF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// The viewmodel of the insert song window.
    /// </summary>
    public class SongViewModel : ViewModelBase
    {
        private SongVM song;
        private BindingList<AlbumVM> albums;
        private AlbumVM selectedAlbum;

        /// <summary>
        /// Initializes a new instance of the <see cref="SongViewModel"/> class.
        /// The constructor of the song viewmodel.
        /// </summary>
        public SongViewModel()
        {
            this.song = new SongVM();
            this.albums = new BindingList<AlbumVM>();
        }

        /// <summary>
        /// Gets or sets the song to be added.
        /// </summary>
        public SongVM Song
        {
            get { return this.song; }
            set { this.song = value; }
        }

        /// <summary>
        /// Gets or sets the album list.
        /// </summary>
        public BindingList<AlbumVM> Albums
        {
            get
            {
                return this.albums;
            }

            set
            {
                this.albums = value;
                this.RaisePropertyChanged(nameof(this.Albums));
                this.RaisePropertyChanged(nameof(this.SelectedAlbum));
            }
        }

        /// <summary>
        /// Gets or sets the selected album.
        /// </summary>
        public AlbumVM SelectedAlbum
        {
            get
            {
                if (this.selectedAlbum == null)
                {
                    return this.Albums.FirstOrDefault();
                }
                else
                {
                    return this.selectedAlbum;
                }
            }

            set
            {
                this.selectedAlbum = value;
            }
        }
    }
}
