﻿// <copyright file="BandViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WPF
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using FelevesFeladat.Data;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// The insert band window's viewmodel.
    /// </summary>
    public class BandViewModel : ViewModelBase
    {
        private BandVM band;

        /// <summary>
        /// Initializes a new instance of the <see cref="BandViewModel"/> class.
        /// The constructor of the band window viewmodel.
        /// </summary>
        public BandViewModel()
        {
            this.band = new BandVM();
        }

        /// <summary>
        /// Gets or sets the band to be added.
        /// </summary>
        public BandVM Band
        {
            get { return this.band; }
            set { this.band = value; }
        }
    }
}
