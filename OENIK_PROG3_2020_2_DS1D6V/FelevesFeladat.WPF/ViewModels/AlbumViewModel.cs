﻿// <copyright file="AlbumViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WPF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// The insert album window viewmodel.
    /// </summary>
    public class AlbumViewModel : ViewModelBase
    {
        private AlbumVM album;

        private BindingList<BandVM> bands;

        private BandVM selectedBand;

        /// <summary>
        /// Initializes a new instance of the <see cref="AlbumViewModel"/> class.
        /// Constructor of the album window's viewmodel.
        /// </summary>
        public AlbumViewModel()
        {
            this.album = new AlbumVM();
            this.bands = new BindingList<BandVM>();

            /*if(IsInDesignMode)
            {
                _album.Title = "Stand Up And Scream";
            }*/
        }

        /// <summary>
        /// Gets or sets the album to be added.
        /// </summary>
        public AlbumVM Album
        {
            get { return this.album; }
            set { this.album = value; }
        }

        /// <summary>
        /// Gets or sets the band list.
        /// </summary>
        public BindingList<BandVM> Bands
        {
            get
            {
                return this.bands;
            }

            set
            {
                this.bands = value;
                this.RaisePropertyChanged(nameof(this.Bands));
                this.RaisePropertyChanged(nameof(this.SelectedBand));
            }
        }

        /// <summary>
        /// Gets or sets the selected band.
        /// </summary>
        public BandVM SelectedBand
        {
            get
            {
                if (this.selectedBand == null)
                {
                    return this.Bands.FirstOrDefault();
                }
                else
                {
                    return this.selectedBand;
                }
            }

            set
            {
                this.selectedBand = value;
            }
        }
    }
}
