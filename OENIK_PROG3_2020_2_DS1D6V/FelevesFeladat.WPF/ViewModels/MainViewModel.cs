﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WPF
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Windows.Controls;
    using System.Windows.Input;
    using FelevesFeladat.Data;
    using FelevesFeladat.Logic;
    using FelevesFeladat.Repository;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// The main window's viewmodel.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Gets or sets a class which contains the methods for database content editing.
        /// </summary>
        private readonly IGetEntityByDefinitionLogicVM getEntityByDefinitionLogic;
        private readonly IEditDataStackLogicVM editDataStackLogic;
        private readonly IModifyEntityLogicVM modifyEntityLogic;

        private BandVM selectedBand;
        private ObservableCollection<BandVM> bands;

        private ObservableCollection<AlbumVM> albums;
        private AlbumVM selectedAlbum;
        private BandVM selectedAlbumsBand;

        private ObservableCollection<SongVM> songs;
        private SongVM selectedSong;
        private AlbumVM selectedSongsAlbum;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// Initializes all the repository,logic and database classes.
        /// </summary>
        /// /// <param name="modifyEntityLogic">The modifyEntity logic interface from the logic layer.</param>
        /// /// <param name="editDataStackLogic">The editDataStackLogic logic interface from the logic layer.</param>
        /// /// <param name="getEntityByDefinitionLogic">The getEntityByDefinitionLogic logic interface from the logic layer.</param>
        public MainViewModel(IGetEntityByDefinitionLogicVM getEntityByDefinitionLogic, IEditDataStackLogicVM editDataStackLogic, IModifyEntityLogicVM modifyEntityLogic)
        {
            this.getEntityByDefinitionLogic = getEntityByDefinitionLogic;
            this.editDataStackLogic = editDataStackLogic;
            this.modifyEntityLogic = modifyEntityLogic;

            this.Bands = this.getEntityByDefinitionLogic.GetBands();
            this.Albums = this.getEntityByDefinitionLogic.GetAlbums();
            this.Songs = this.getEntityByDefinitionLogic.GetSongs();

            this.SelectedBand = this.getEntityByDefinitionLogic.GetSelectedBand(this.Bands);
            this.SelectedAlbum = this.getEntityByDefinitionLogic.GetSelectedAlbum(this.Albums);
            this.SelectedSong = this.getEntityByDefinitionLogic.GetSelectedSong(this.Songs);

            this.AddBandCmd = new RelayCommand(() =>
            {
                this.editDataStackLogic.InsertBand(this.Bands, ref this.selectedBand);
                this.SelectedBand = this.selectedBand;
            });
            this.RemoveBandCmd = new RelayCommand(() =>
            {
                this.editDataStackLogic.RemoveBand(ref this.selectedBand, this.Bands);
                this.Albums = this.getEntityByDefinitionLogic.GetAlbums();
                this.Songs = this.getEntityByDefinitionLogic.GetSongs();
                this.SelectedBand = this.selectedBand;
            });
            this.SaveBandCmd = new RelayCommand(() => this.modifyEntityLogic.SaveBand(this.SelectedBand));
            this.AddAlbumCmd = new RelayCommand(() =>
            {
                this.editDataStackLogic.InsertAlbum(this.Albums, this.Bands, ref this.selectedAlbum);
                this.SelectedAlbum = this.selectedAlbum;
            });
            this.RemoveAlbumCmd = new RelayCommand(() =>
            {
                this.editDataStackLogic.RemoveAlbum(ref this.selectedAlbum, this.Albums);
                this.Songs = this.getEntityByDefinitionLogic.GetSongs();
                this.SelectedAlbum = this.Albums.FirstOrDefault();
            });
            this.SaveAlbumCmd = new RelayCommand(() =>
            {
                this.selectedAlbumsBand = this.SelectedAlbumsBand;

                this.modifyEntityLogic.SaveAlbum(this.SelectedAlbum, this.selectedAlbumsBand);
            });
            this.AddSongCmd = new RelayCommand(() =>
            {
                this.editDataStackLogic.InsertSong(this.Albums, this.Songs, ref this.selectedSong);
                this.SelectedSong = this.selectedSong;
            });
            this.RemoveSongCmd = new RelayCommand(() =>
            {
                this.editDataStackLogic.RemoveSong(this.Songs, ref this.selectedSong);
                this.SelectedSong = this.Songs.FirstOrDefault();
            });
            this.SaveSongCmd = new RelayCommand(() =>
            {
                this.selectedSongsAlbum = this.SelectedSongsAlbum;

                this.modifyEntityLogic.SaveSong(this.SelectedSong, this.selectedSongsAlbum);
            });
        }

        /// <summary>
        /// Gets or sets the band list.
        /// </summary>
        public ObservableCollection<BandVM> Bands
        {
            get { return this.bands; }
            set { this.Set(ref this.bands, value); }
        }

        /// <summary>
        /// Gets or sets the selected band.
        /// </summary>
        public BandVM SelectedBand
        {
            get
            {
                return this.selectedBand;
            }

            set
            {
                this.Set(ref this.selectedBand, value);
                this.RaisePropertyChanged(nameof(this.SelectedBand));
            }
        }

        /// <summary>
        /// Gets or sets the album list.
        /// </summary>
        public ObservableCollection<AlbumVM> Albums
        {
            get { return this.albums; }
            set { this.Set(ref this.albums, value); }
        }

        /// <summary>
        /// Gets or sets the selected album.
        /// </summary>
        public AlbumVM SelectedAlbum
        {
            get
            {
                return this.selectedAlbum;
            }

            set
            {
                this.Set(ref this.selectedAlbum, value);
                this.RaisePropertyChanged(nameof(this.SelectedAlbumsBand));
                this.RaisePropertyChanged(nameof(this.SelectedAlbum));
            }
        }

        /// <summary>
        /// Gets or sets the album taht has the selected album.
        /// </summary>
        public BandVM SelectedAlbumsBand
        {
            get
            {
                if (this.selectedAlbum == null)
                {
                    return null;
                }

                return this.selectedAlbum.Band;
            }

            set
            {
                this.Set(ref this.selectedAlbumsBand, value);
                this.selectedAlbum.Band = this.selectedAlbumsBand;
            }
        }

        /// <summary>
        /// Gets or sets the song list.
        /// </summary>
        public ObservableCollection<SongVM> Songs
        {
            get { return this.songs; }
            set { this.Set(ref this.songs, value); }
        }

        /// <summary>
        /// Gets or sets the selected song.
        /// </summary>
        public SongVM SelectedSong
        {
            get
            {
                return this.selectedSong;
            }

            set
            {
                this.Set(ref this.selectedSong, value);
                this.RaisePropertyChanged(nameof(this.SelectedSongsAlbum));
                this.RaisePropertyChanged(nameof(this.SelectedSong));
            }
        }

        /// <summary>
        /// Gets or sets the album of the selected song.
        /// </summary>
        public AlbumVM SelectedSongsAlbum
        {
            get
            {
                if (this.selectedSong == null)
                {
                    return null;
                }

                return this.selectedSong.Album;
            }

            set
            {
                this.Set(ref this.selectedSongsAlbum, value);
                this.selectedSong.Album = this.SelectedSongsAlbum;
            }
        }

        /// <summary>
        /// Gets the command that adds a band.
        /// </summary>
        public ICommand AddBandCmd { get; private set; }

        /// <summary>
        /// Gets the command that removes a band.
        /// </summary>
        public ICommand RemoveBandCmd { get; private set; }

        /// <summary>
        /// Gets the command that saves a band.
        /// </summary>
        public ICommand SaveBandCmd { get; private set; }

        /// <summary>
        /// Gets the command that adds an album.
        /// </summary>
        public ICommand AddAlbumCmd { get; private set; }

        /// <summary>
        /// Gets the command that removes an album.
        /// </summary>
        public ICommand RemoveAlbumCmd { get; private set; }

        /// <summary>
        /// Gets the command that saves an album.
        /// </summary>
        public ICommand SaveAlbumCmd { get; private set; }

        /// <summary>
        /// Gets the command that adds a song.
        /// </summary>
        public ICommand AddSongCmd { get; private set; }

        /// <summary>
        /// Gets the command that removes a song.
        /// </summary>
        public ICommand RemoveSongCmd { get; private set; }

        /// <summary>
        /// Gets the command that saves a song.
        /// </summary>
        public ICommand SaveSongCmd { get; private set; }
    }
}
