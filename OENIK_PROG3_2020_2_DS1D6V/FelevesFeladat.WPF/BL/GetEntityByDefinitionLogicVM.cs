﻿// <copyright file="GetEntityByDefinitionLogicVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WPF
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FelevesFeladat.Logic;

    /// <summary>
    /// Makes the connection between the logic and the view layer.
    /// </summary>
    internal class GetEntityByDefinitionLogicVM : IGetEntityByDefinitionLogicVM
    {
        private readonly IGetEntityByDefinitionLogic getEntityByDefinitionLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetEntityByDefinitionLogicVM"/> class.
        /// The contructor that contains the dependeny injections from the logic layer.
        /// </summary>
        /// <param name="getEntityByDefinitionLogic">getEntityByDefinitionLogic interface from the logic layer.</param>
        public GetEntityByDefinitionLogicVM(IGetEntityByDefinitionLogic getEntityByDefinitionLogic)
        {
            this.getEntityByDefinitionLogic = getEntityByDefinitionLogic;
        }

        /// <inheritdoc/>
        public ObservableCollection<AlbumVM> GetAlbums()
        {
            ObservableCollection<AlbumVM> albums = new ObservableCollection<AlbumVM>(this.getEntityByDefinitionLogic.GetAllAlbums().Select(x => new AlbumVM(x)));

            return albums;
        }

        /// <inheritdoc/>
        public ObservableCollection<SongVM> GetSongs()
        {
            ObservableCollection<SongVM> songs = new ObservableCollection<SongVM>(this.getEntityByDefinitionLogic.GetAllSongs().Select(x => new SongVM(x)));

            return songs;
        }

        /// <inheritdoc/>
        public ObservableCollection<BandVM> GetBands()
        {
            ObservableCollection<BandVM> bands = new ObservableCollection<BandVM>(this.getEntityByDefinitionLogic.GetAllBands().Select(x => new BandVM(x)));

            return bands;
        }

        /// <inheritdoc/>
        public BandVM GetSelectedBand(ObservableCollection<BandVM> bands)
        {
            BandVM selectedBand = new BandVM();

            if (bands.Count == 0)
            {
                selectedBand = null;
            }
            else
            {
                selectedBand = bands.First();
            }

            return selectedBand;
        }

        /// <inheritdoc/>
        public AlbumVM GetSelectedAlbum(ObservableCollection<AlbumVM> albums)
        {
            AlbumVM selectedAlbum = new AlbumVM();

            if (albums.Count == 0)
            {
                selectedAlbum = null;
            }
            else
            {
                selectedAlbum = albums.First();
            }

            return selectedAlbum;
        }

        /// <inheritdoc/>
        public SongVM GetSelectedSong(ObservableCollection<SongVM> songs)
        {
            SongVM selectedSong = new SongVM();

            if (songs.Count == 0)
            {
                selectedSong = null;
            }
            else
            {
                selectedSong = songs.First();
            }

            return selectedSong;
        }
    }
}
