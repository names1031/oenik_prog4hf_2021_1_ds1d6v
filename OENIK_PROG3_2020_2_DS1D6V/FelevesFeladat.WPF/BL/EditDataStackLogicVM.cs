﻿// <copyright file="EditDataStackLogicVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WPF
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FelevesFeladat.Logic;

    /// <summary>
    /// Makes the connection between the logic and the view layer.
    /// </summary>
    internal class EditDataStackLogicVM : IEditDataStackLogicVM
    {
        private readonly IGetEntityByDefinitionLogic getEntityByDefinitionLogic;
        private readonly IEditDataStackLogic editDataStackLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditDataStackLogicVM"/> class.
        /// The contructor that contains the dependeny injections from the logic layer.
        /// </summary>
        /// <param name="editDataStackLogic">The editDataStackLogic interface from the logic layer.</param>
        /// <param name="getEntityByDefinitionLogic">The getEntityByDefinitionLogic interface from the logic layer.</param>
        public EditDataStackLogicVM(IEditDataStackLogic editDataStackLogic, IGetEntityByDefinitionLogic getEntityByDefinitionLogic)
        {
            this.editDataStackLogic = editDataStackLogic;
            this.getEntityByDefinitionLogic = getEntityByDefinitionLogic;
        }

        /// <inheritdoc/>
        public void InsertAlbum(ObservableCollection<AlbumVM> albums, ObservableCollection<BandVM> bands, ref AlbumVM selectedAlbum)
        {
            InsertAlbumWindow albumWindow = new InsertAlbumWindow();
            albumWindow.VM.Bands = new BindingList<BandVM>(bands);
            if (albumWindow.ShowDialog() == true)
            {
                AlbumVM addedAlbum = albumWindow.VM.Album;
                BandVM selectedBand = albumWindow.VM.SelectedBand;
                this.editDataStackLogic.InsertAlbum(addedAlbum.Title, addedAlbum.Released, addedAlbum.Language, addedAlbum.Length, addedAlbum.Label, selectedBand.ID);
                albums.Add(new AlbumVM(this.getEntityByDefinitionLogic.GetAllAlbums().Last()));
                selectedAlbum = albums.Last();
            }
        }

        /// <inheritdoc/>
        public void InsertBand(ObservableCollection<BandVM> bands, ref BandVM selectedBand)
        {
            InsertBandWindows bandWindow = new InsertBandWindows();
            if (bandWindow.ShowDialog() == true)
            {
                BandVM addedBand = bandWindow.VM.Band;
                this.editDataStackLogic.InsertBand(addedBand.Name, addedBand.Origin, addedBand.Members, addedBand.Genres, addedBand.Formed);
                bands.Add(new BandVM(this.getEntityByDefinitionLogic.GetAllBands().Last()));
                selectedBand = bands.Last();
            }
        }

        /// <inheritdoc/>
        public void InsertSong(ObservableCollection<AlbumVM> albums, ObservableCollection<SongVM> songs, ref SongVM selectedSong)
        {
            InsertSongWindow songWindow = new InsertSongWindow();
            songWindow.VM.Albums = new BindingList<AlbumVM>(albums);
            if (songWindow.ShowDialog() == true)
            {
                SongVM addedSong = songWindow.VM.Song;
                AlbumVM selectedAlbum = songWindow.VM.SelectedAlbum;
                this.editDataStackLogic.InsertSong(addedSong.Name, addedSong.MusicVideo.ToString(), addedSong.Length, addedSong.GuitarSolo.ToString(), addedSong.Billboard, selectedAlbum.ID);
                songs.Add(new SongVM(this.getEntityByDefinitionLogic.GetAllSongs().Last()));
                selectedSong = songs.Last();
            }
        }

        /// <inheritdoc/>
        public void RemoveAlbum(ref AlbumVM selectedAlbum, ObservableCollection<AlbumVM> albums)
        {
            if (selectedAlbum == null)
            {
                return;
            }

            this.editDataStackLogic.RemoveAlbum(selectedAlbum.ID);

            albums.Remove(selectedAlbum);
            selectedAlbum = albums.FirstOrDefault();
        }

        /// <inheritdoc/>
        public void RemoveBand(ref BandVM selectedBand, ObservableCollection<BandVM> bands)
        {
            if (selectedBand == null)
            {
                return;
            }

            if (!string.IsNullOrEmpty(selectedBand.ID))
            {
                this.editDataStackLogic.RemoveBand(selectedBand.ID);
            }

            bands.Remove(selectedBand);
            selectedBand = bands.FirstOrDefault();
        }

        /// <inheritdoc/>
        public void RemoveSong(ObservableCollection<SongVM> songs, ref SongVM selectedSong)
        {
            if (selectedSong == null)
            {
                return;
            }
            else
            {
                this.editDataStackLogic.RemoveSong(selectedSong.ID);
            }

            songs.Remove(selectedSong);
            selectedSong = songs.FirstOrDefault();
        }
    }
}
