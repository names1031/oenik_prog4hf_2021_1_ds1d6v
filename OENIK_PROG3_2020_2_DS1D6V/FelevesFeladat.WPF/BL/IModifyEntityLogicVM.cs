﻿// <copyright file="IModifyEntityLogicVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WPF
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The interface for the getEntityByDefinition class.
    /// </summary>
    public interface IModifyEntityLogicVM
    {
        /// <summary>
        /// Calls the updateBand method in the logic layer.
        /// </summary>
        /// <param name="selectedBand">The selected band.</param>
        void SaveBand(BandVM selectedBand);

        /// <summary>
        /// Calls the updateAlbum method in the logic layer.
        /// </summary>
        /// <param name="selectedAlbum">The selected album.</param>
        /// <param name="selectedAlbumsBand">The selected album's band.</param>
        void SaveAlbum(AlbumVM selectedAlbum, BandVM selectedAlbumsBand);

        /// <summary>
        /// Calls the updateSong method in the logic layer.
        /// </summary>
        /// <param name="selectedSong">The selected song.</param>
        /// <param name="selectedSongsAlbum">The selected song's album.</param>
        void SaveSong(SongVM selectedSong, AlbumVM selectedSongsAlbum);
    }
}
