﻿// <copyright file="ModifyEntityLogicVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WPF
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FelevesFeladat.Logic;

    /// <summary>
    /// Makes the connection between the logic and the view layer.
    /// </summary>
    internal class ModifyEntityLogicVM : IModifyEntityLogicVM
    {
        private readonly IModifyEntityLogic modifyEntityLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModifyEntityLogicVM"/> class.
        /// The contructor that contains the dependeny injections from the logic layer.
        /// </summary>
        /// <param name="modifyEntityLogic">The modifyEntity logic interface from the logic layer.</param>
        public ModifyEntityLogicVM(IModifyEntityLogic modifyEntityLogic)
        {
            this.modifyEntityLogic = modifyEntityLogic;
        }

        /// <inheritdoc/>
        public void SaveBand(BandVM selectedBand)
        {
            if (selectedBand == null)
            {
                return;
            }

            if (!string.IsNullOrEmpty(selectedBand.ID))
            {
                this.modifyEntityLogic.UpdateBand(selectedBand.ID, selectedBand.Name, selectedBand.Formed, selectedBand.Genres, selectedBand.Members, selectedBand.Origin);
            }
        }

        /// <inheritdoc/>
        public void SaveAlbum(AlbumVM selectedAlbum, BandVM selectedAlbumsBand)
        {
            if (selectedAlbum == null)
            {
                return;
            }

            if (!string.IsNullOrEmpty(selectedAlbum.ID))
            {
                this.modifyEntityLogic.UpdateAlbum(selectedAlbum.ID, selectedAlbum.Title, selectedAlbum.Released, selectedAlbum.Language, selectedAlbum.Length, selectedAlbum.Label, selectedAlbumsBand.ID);
            }
        }

        /// <inheritdoc/>
        public void SaveSong(SongVM selectedSong, AlbumVM selectedSongsAlbum)
        {
            if (selectedSong == null)
            {
                return;
            }

            this.modifyEntityLogic.UpdateSong(selectedSong.ID, selectedSong.Name, selectedSong.MusicVideo, selectedSong.Length, selectedSong.GuitarSolo, selectedSong.Billboard, selectedSongsAlbum.ID);
        }
    }
}
