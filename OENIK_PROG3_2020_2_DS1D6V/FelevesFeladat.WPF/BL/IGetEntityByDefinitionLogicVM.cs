﻿// <copyright file="IGetEntityByDefinitionLogicVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WPF
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The interface for the getEntityByDefinition class.
    /// </summary>
    public interface IGetEntityByDefinitionLogicVM
    {
        /// <summary>
        /// Returns all the bands in the database.
        /// </summary>
        /// <returns>A list with all the bands.</returns>
        ObservableCollection<BandVM> GetBands();

        /// <summary>
        /// Returns all the albums in the database.
        /// </summary>
        /// <returns>A list with all the albums.</returns>
        ObservableCollection<AlbumVM> GetAlbums();

        /// <summary>
        /// Returns all the songs in the database.
        /// </summary>
        /// <returns>A list with all the songs.</returns>
        ObservableCollection<SongVM> GetSongs();

        /// <summary>
        /// Return the default selected band in the listbox.
        /// </summary>
        /// <param name="bands">The band list.</param>
        /// <returns>The band model.</returns>
        BandVM GetSelectedBand(ObservableCollection<BandVM> bands);

        /// <summary>
        /// Returns the default selected album in the listbox.
        /// </summary>
        /// <param name="albums">The album list.</param>
        /// <returns>The album model.</returns>
        AlbumVM GetSelectedAlbum(ObservableCollection<AlbumVM> albums);

        /// <summary>
        /// Return the default selected song in the listbox.
        /// </summary>
        /// <param name="songs">The song list.</param>
        /// <returns>The song model.</returns>
        SongVM GetSelectedSong(ObservableCollection<SongVM> songs);
    }
}
