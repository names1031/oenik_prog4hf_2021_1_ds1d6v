﻿// <copyright file="IEditDataStackLogicVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WPF
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The interface for the editdatastacklogicVM class.
    /// </summary>
    public interface IEditDataStackLogicVM
    {
        /// <summary>
        /// Calls the insert band method in the logic layer with the band model.
        /// </summary>
        /// <param name="bands">The band list that the selected band will be inserted.</param>
        /// <param name="selectedBand">The selected band.</param>
        void InsertBand(ObservableCollection<BandVM> bands, ref BandVM selectedBand);

        /// <summary>
        /// Calls the insert album method in the logic layer with the album model.
        /// </summary>
        /// <param name="albums">The album list which the selected album will be inserted.</param>
        /// <param name="bands">The bands list from the albums's band will be selected.</param>
        /// <param name="selectedAlbum">The selected album.</param>
        void InsertAlbum(ObservableCollection<AlbumVM> albums, ObservableCollection<BandVM> bands, ref AlbumVM selectedAlbum);

        /// <summary>
        /// Calls the insert song method in the logic layer with the song model.
        /// </summary>
        /// <param name="albums">The album list from the song's album will be selected.</param>
        /// <param name="songs">The song list which the selected song will be inserted.</param>
        /// <param name="selectedSong">The selected song.</param>
        void InsertSong(ObservableCollection<AlbumVM> albums, ObservableCollection<SongVM> songs, ref SongVM selectedSong);

        /// <summary>
        /// Calls the remove method in the logic layer with the band model.
        /// </summary>
        /// <param name="selectedBand">The selected band.</param>
        /// <param name="bands">The band list.</param>
        void RemoveBand(ref BandVM selectedBand, ObservableCollection<BandVM> bands);

        /// <summary>
        /// Calls the remove method in the logic layer with the album model.
        /// </summary>
        /// <param name="selectedAlbum">The selected album.</param>
        /// <param name="albums">The album list.</param>
        void RemoveAlbum(ref AlbumVM selectedAlbum, ObservableCollection<AlbumVM> albums);

        /// <summary>
        /// Calls the remove method in the logic layer with the song model.
        /// </summary>
        /// <param name="songs">The song list.</param>
        /// <param name="selectedSong">The selected song.</param>
        void RemoveSong(ObservableCollection<SongVM> songs, ref SongVM selectedSong);
    }
}
