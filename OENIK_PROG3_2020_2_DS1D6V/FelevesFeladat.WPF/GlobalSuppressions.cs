﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "Not necessary", Scope = "member", Target = "~M:FelevesFeladat.WPF.AlbumVM.GetHashCode~System.Int32")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "Not necessary", Scope = "member", Target = "~M:FelevesFeladat.WPF.BandVM.GetHashCode~System.Int32")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Not necessary", Scope = "member", Target = "~P:FelevesFeladat.WPF.AlbumViewModel.Bands")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Not necessary", Scope = "member", Target = "~P:FelevesFeladat.WPF.MainViewModel.Bands")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Not necessary", Scope = "member", Target = "~P:FelevesFeladat.WPF.MainViewModel.Albums")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Not necessary", Scope = "member", Target = "~P:FelevesFeladat.WPF.MainViewModel.Songs")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Not necessary", Scope = "member", Target = "~P:FelevesFeladat.WPF.SongViewModel.Albums")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "Not necessary", Scope = "type", Target = "~T:FelevesFeladat.WPF.App")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Justification = "Late bound")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1014:MarkAssembliesWithClsCompliant", Justification = "Late bound")]
