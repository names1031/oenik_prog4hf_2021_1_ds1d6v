﻿// <copyright file="AlbumVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WPF
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FelevesFeladat.Data;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// The albumVM class that is used in the view layer.
    /// </summary>
    public class AlbumVM : ObservableObject
    {
        private readonly Album album;
        private string id;
        private string released = "01.01.0001";
        private string title;
        private string language;
        private string length = "1.0";
        private string label;
        private string bandId = "1";
        private BandVM band;

        /// <summary>
        /// Initializes a new instance of the <see cref="AlbumVM"/> class.
        /// The constructor of the album class.
        /// </summary>
        public AlbumVM()
        {
            this.album = new Album();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AlbumVM"/> class.
        /// The constructor that converts an album entity to an albumVm entity.
        /// </summary>
        /// <param name="album">The album entity.</param>
        public AlbumVM(Album album)
        {
            /* Reflexióval:
             this.GetType().GetProperties().ToList().ForEach(property => property.SetValue(this,property.GetValue(album)));*/

            this.album = album;
            this.id = this.album.ID.ToString(new CultureInfo("en"));
            this.label = this.album.Label;
            this.language = this.album.Language;
            this.length = this.album.Length.ToString(new CultureInfo("en"));
            this.released = this.album.Released.ToString(new CultureInfo("en"));
            this.title = this.album.Title;
            this.bandId = this.album.BandID.ToString(new CultureInfo("en"));
            this.band = new BandVM(this.album.Band);
        }

        /// <summary>
        /// Gets or sets the album's id.
        /// </summary>
        public string ID
        {
            get { return this.id; }
            set { this.Set(ref this.id, value); }
        }

        /// <summary>
        /// Gets or sets when the album was released.
        /// </summary>
        public string Released
        {
            get { return this.released; }
            set { this.Set(ref this.released, value); }
        }

        /// <summary>
        /// Gets or sets the album's title.
        /// </summary>
        public string Title
        {
            get { return this.title; }
            set { this.Set(ref this.title, value); }
        }

        /// <summary>
        /// Gets or sets the album's language.
        /// </summary>
        public string Language
        {
            get { return this.language; }
            set { this.Set(ref this.language, value); }
        }

        /// <summary>
        /// Gets or sets the album's length.
        /// </summary>
        public string Length
        {
            get { return this.length; }
            set { this.Set(ref this.length, value); }
        }

        /// <summary>
        /// Gets or sets the album's label.
        /// </summary>
        public string Label
        {
            get { return this.label; }
            set { this.Set(ref this.label, value); }
        }

        /// <summary>
        /// Gets or sets the band's id that has the album.
        /// </summary>
        public string BandId
        {
            get { return this.bandId; }
            set { this.Set(ref this.bandId, value); }
        }

        /// <summary>
        /// Gets or sets the band that has the album.
        /// </summary>
        public BandVM Band
        {
            get { return this.band; }
            set { this.Set(ref this.band, value); }
        }

        /// <summary>
        /// The method that converts an albuMV entity to an album entity.
        /// </summary>
        /// <returns>An album entity.</returns>
        public Album ToAlbum()
        {
            this.album.ID = int.Parse(this.id, new CultureInfo("en"));
            this.album.Label = this.label;
            this.album.Language = this.language;
            this.album.Length = double.Parse(this.length, new CultureInfo("en"));
            this.album.Released = DateTime.Parse(this.released, new CultureInfo("en"));
            this.album.Title = this.title;
            this.album.BandID = int.Parse(this.bandId, new CultureInfo("en"));
            this.album.Band = this.band.ToBand();

            return this.album;
        }

        /// <summary>
        /// The override of the equals method.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>Whether this object equals the other object.</returns>
        public override bool Equals(object obj)
        {
            AlbumVM other = obj as AlbumVM;
            if (other is null)
            {
                return false;
            }

            return other.ID == this.ID;
        }

        /// <summary>
        /// The override of the getHashCode method.
        /// </summary>
        /// <returns>This object's hashCode.</returns>
        public override int GetHashCode()
        {
            return this.id.GetHashCode();
        }
    }
}
