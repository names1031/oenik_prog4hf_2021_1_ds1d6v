﻿// <copyright file="SongVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WPF
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FelevesFeladat.Data;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// The SongVM class that is used in the view layer.
    /// </summary>
    public class SongVM : ObservableObject
    {
        private readonly Song song;
        private string id;
        private string name;
        private bool musicVideo;
        private string length = "1.0";
        private bool guitarSolo;
        private string billboard;
        private string albumID;
        private AlbumVM album;

        /// <summary>
        /// Initializes a new instance of the <see cref="SongVM"/> class.
        /// The songVM constructor.
        /// </summary>
        public SongVM()
        {
            this.song = new Song();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SongVM"/> class.
        /// The constructor that converts a Song entity to a SongVM entity.
        /// </summary>
        /// <param name="song">The Song entity.</param>
        public SongVM(Song song)
        {
        /*Reflexióval:
            this.GetType().GetProperties().ToList().ForEach(
                property => property.SetValue(this, property.GetValue(song))
                );*/

            this.song = song;
            this.id = this.song.ID.ToString(new CultureInfo("en"));
            this.length = this.song.Length.ToString(new CultureInfo("en"));
            this.musicVideo = this.song.MusicVideo;
            this.name = this.song.Name;
            this.guitarSolo = this.song.GuitarSolo;
            this.billboard = this.song.Billboard?.ToString(new CultureInfo("en"));
            this.albumID = this.song.Album.ID.ToString(new CultureInfo("en"));
            this.album = new AlbumVM(this.song.Album);
        }

        /// <summary>
        /// Gets or sets the song's id.
        /// </summary>
        public string ID
        {
            get { return this.id; }
            set { this.Set(ref this.id, value); }
        }

        /// <summary>
        /// Gets or sets the song's name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the song has music video.
        /// </summary>
        public bool MusicVideo
        {
            get { return this.musicVideo; }
            set { this.Set(ref this.musicVideo, value); }
        }

        /// <summary>
        /// Gets or sets the song's length.
        /// </summary>
        public string Length
        {
            get { return this.length; }
            set { this.Set(ref this.length, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the song has guitar solo.
        /// </summary>
        public bool GuitarSolo
        {
            get { return this.guitarSolo; }
            set { this.Set(ref this.guitarSolo, value); }
        }

        /// <summary>
        /// Gets or sets the song's billboard placement.
        /// </summary>
        public string Billboard
        {
            get { return this.billboard; }
            set { this.Set(ref this.billboard, value); }
        }

        /// <summary>
        /// Gets or sets the album' id that has the song.
        /// </summary>
        public string AlbumID
        {
            get { return this.albumID; }
            set { this.Set(ref this.albumID, value); }
        }

        /// <summary>
        /// Gets or sets the album that has the song.
        /// </summary>
        public AlbumVM Album
        {
            get { return this.album; }
            set { this.Set(ref this.album, value); }
        }

        /// <summary>
        /// The method that converts a SongVM entity to a Song entity.
        /// </summary>
        /// <returns>The song entity.</returns>
        public Song ToSong()
        {
            this.song.AlbumID = int.Parse(this.albumID, new CultureInfo("en"));
            this.song.Billboard = int.Parse(this.billboard, new CultureInfo("en"));
            this.song.GuitarSolo = this.guitarSolo;
            this.song.ID = int.Parse(this.id, new CultureInfo("en"));
            this.song.Length = double.Parse(this.length, new CultureInfo("en"));
            this.song.MusicVideo = this.musicVideo;
            this.song.Name = this.name;
            this.song.Album = this.album.ToAlbum();

            return this.song;
        }
    }
}
