﻿// <copyright file="LengthToString.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WPF
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Controls;
    using System.Windows.Data;

    /// <summary>
    /// The class that converts the length double variable to a formated string.
    /// </summary>
    internal class LengthToString : IValueConverter
    {
        /// <summary>
        /// Convert from viewmodel to UI.
        /// </summary>
        /// <param name="value">The object that is converted.</param>
        /// <param name="targetType">The object's type which the other object is converted to.</param>
        /// <param name="parameter">The xaml converter parameter.</param>
        /// <param name="culture">The culture info of the converting.</param>
        /// <returns>The converted object.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double length = double.Parse((string)value, new CultureInfo("en"));

            string[] secString = length.ToString(new CultureInfo("en")).Split('.');

            int sec = 0;

            if (secString.Length != 1)
            {
                sec = int.Parse(secString[1], new CultureInfo("en"));
            }

            return $"{Math.Floor((double)length / 60)}.h {Math.Floor((double)length) % 60}.m {sec}.s";
        }

        /// <summary>
        /// Covert from UI to viewmodel.
        /// </summary>
        /// <param name="value">The object that is converted back.</param>
        /// <param name="targetType">The object's type which the other object is converted back to.</param>
        /// <param name="parameter">The xaml converter parameter.</param>
        /// <param name="culture">The culture info of the converting.</param>
        /// <returns>The converted object.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string[] input = value.ToString().Split(' ');
            double number = 0;

            for (int i = 0; i < input.Length; i++)
            {
                int numberInput = int.Parse(input[i].Split('.')[0], new CultureInfo("en"));

                if (i == 0)
                {
                    number += numberInput * 60;
                }
                else if (i == 1)
                {
                    number += numberInput;
                }
                else
                {
                    number += double.Parse("0." + numberInput.ToString(new CultureInfo("en")), new CultureInfo("en"));
                }
            }

            return number;
        }
    }
}
