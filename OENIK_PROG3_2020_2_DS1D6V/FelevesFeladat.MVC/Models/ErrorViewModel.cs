// <copyright file="ErrorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.MVC.Models
{
    /// <summary>
    /// The error viewmodel class.
    /// </summary>
    public class ErrorViewModel
    {
        /// <summary>
        /// Gets or sets the erroviewmodel's requested id.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Gets a value indicating whether the requested id is null or not.
        /// </summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(this.RequestId);
    }
}
