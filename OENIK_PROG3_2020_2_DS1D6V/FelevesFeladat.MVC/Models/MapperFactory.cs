﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.MVC.Models
{
    using AutoMapper;

    /// <summary>
    /// The mapper factory class.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Creates the auto mapper.
        /// </summary>
        /// <returns>The mapper interface.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.Band, MVC.Models.Band>()
                .ForMember(dest => dest.ID, map => map.MapFrom(src => src.ID))
                .ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name))
                .ForMember(dest => dest.Origin, map => map.MapFrom(src => src.Origin))
                .ForMember(dest => dest.Members, map => map.MapFrom(src => src.Members))
                .ForMember(dest => dest.Genres, map => map.MapFrom(src => src.Genres))
                .ForMember(dest => dest.Formed, map => map.MapFrom(src => src.Formed))
                /*.ReverseMap()*/;
            });

            return config.CreateMapper();
        }
    }
}
