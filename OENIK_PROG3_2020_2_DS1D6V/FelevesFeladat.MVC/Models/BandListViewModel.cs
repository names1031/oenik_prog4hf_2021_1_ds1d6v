﻿// <copyright file="BandListViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.MVC.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// The viewmodel of the bands list.
    /// </summary>
    public class BandListViewModel
    {
        /// <summary>
        /// Gets or sets the list of the bands.
        /// </summary>
        public List<Band> Bands { get; set; }

        /// <summary>
        /// Gets or sets the current edited band.
        /// </summary>
        public Band EditedBand { get; set; }
    }
}
