﻿// <copyright file="Band.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.MVC.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The band class.
    /// </summary>
    public class Band
    {
        /// <summary>
        /// Gets or sets the band's id.
        /// </summary>
        [Display(Name = "Band id: ")]
        [Required]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the name of the band.
        /// </summary>
        [Required]
        [Display(Name = "Band name: ")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the origin of the band.
        /// </summary>
        [Required]
        [Display(Name = "Band Origin: ")]
        public string Origin { get; set; }

        /// <summary>
        /// Gets or sets the members of the band.
        /// </summary>
        [Required]
        [Display(Name = "Band members: ")]
        public string Members { get; set; }

        /// <summary>
        /// Gets or sets the band's genre of music.
        /// </summary>
        [Required]
        [StringLength(130, MinimumLength = 2)]
        [Display(Name = "Band genres: ")]
        public string Genres { get; set; }

        /// <summary>
        /// Gets or sets the datetime when the band was formed.
        /// </summary>
        [Required]
        [Display(Name = "Band formed: ")]
        public DateTime Formed { get; set; }
    }
}
