﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "Unnecessary warning.", Scope = "member", Target = "~P:FelevesFeladat.MVC.Models.BandListViewModel.Bands")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Unnecessary warning.", Scope = "member", Target = "~P:FelevesFeladat.MVC.Models.BandListViewModel.Bands")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1014:MarkAssembliesWithClsCompliant", Justification = "Late bound")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Unnecessary warning.", Scope = "member", Target = "~M:FelevesFeladat.MVC.Controllers.BandsApiController.AddOneBand(FelevesFeladat.MVC.Models.Band)~FelevesFeladat.MVC.Controllers.ApiResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Unnecessary warning", Scope = "member", Target = "~M:FelevesFeladat.MVC.Controllers.BandsApiController.ModifyOneBand(FelevesFeladat.MVC.Models.Band)~FelevesFeladat.MVC.Controllers.ApiResult")]
