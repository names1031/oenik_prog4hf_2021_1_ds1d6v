﻿// <copyright file="BandsApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.MVC.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using AutoMapper;
    using FelevesFeladat.Logic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The band's api controller class.
    /// </summary>
    public class BandsApiController : Controller
    {
        private IEditDataStackLogic editLogic;
        private IGetEntityByDefinitionLogic getLogic;
        private IModifyEntityLogic modLogic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="BandsApiController"/> class.
        /// Constructor of the api controller.
        /// </summary>
        /// <param name="editLogic">The edit logic.</param>
        /// <param name="getLogic">The get logic.</param>
        /// <param name="modLogic">The modifier logic.</param>
        /// <param name="mapper">The automapper.</param>
        public BandsApiController(IEditDataStackLogic editLogic, IGetEntityByDefinitionLogic getLogic, IModifyEntityLogic modLogic, IMapper mapper)
        {
            this.editLogic = editLogic;
            this.getLogic = getLogic;
            this.modLogic = modLogic;
            this.mapper = mapper;
        }

        // Get BandsApi/all

        /// <summary>
        /// Gets all the bands.
        /// </summary>
        /// <returns>The bands in a list.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Band> GetAll()
        {
            var bands = this.getLogic.GetAllBands();
            return this.mapper.Map<IList<Data.Band>, List<Models.Band>>(bands);
        }

        // Get BandsApi/del/5

        /// <summary>
        /// The delete band method.
        /// </summary>
        /// <param name="id">The to be deleted band's id.</param>
        /// <returns>An ApiResult class as result of the action.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOneCar(int id)
        {
            this.editLogic.RemoveBand(id.ToString(new CultureInfo("en")));
            return new ApiResult() { OperationResult = true };
        }

        // Post BandsApi/add + Post 1 band

        /// <summary>
        /// The insert band method.
        /// </summary>
        /// <param name="band">The to be inserted band.</param>
        /// <returns>The ApiResult class as the result of the action.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneBand(Models.Band band)
        {
            bool succes = true;
            try
            {
                this.editLogic.InsertBand(band.Name, band.Origin, band.Members, band.Genres, band.Formed.ToString(new CultureInfo("en")));
            }
            catch (ArgumentException)
            {
                succes = false;
            }

            return new ApiResult() { OperationResult = succes };
        }

        // Post BandsApi/mod + Post 1 band

        /// <summary>
        /// The modify band method.
        /// </summary>
        /// <param name="band">The to be modified band.</param>
        /// <returns>The ApiResult class as the result of the action.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModifyOneBand(Models.Band band)
        {
            this.modLogic.UpdateBand(band.ID.ToString(new CultureInfo("en")), band.Name, band.Formed.ToString(new CultureInfo("en")), band.Genres, band.Members, band.Origin);
            return new ApiResult() { OperationResult = true };
        }
    }
}