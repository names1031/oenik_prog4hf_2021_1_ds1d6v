﻿// <copyright file="ApiResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.MVC.Controllers
{
    using System.Collections.Generic;
    using AutoMapper;
    using FelevesFeladat.Logic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The result class for the api controller.
    /// </summary>
    public class ApiResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether the result of the operation is true.
        /// </summary>
        public bool OperationResult { get; set; }
    }
}