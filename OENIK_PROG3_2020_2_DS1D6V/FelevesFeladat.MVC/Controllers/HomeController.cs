﻿// <copyright file="HomeController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.MVC.Controllers
{
    using System.Diagnostics;
    using FelevesFeladat.MVC.Models;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// The home controller class.
    /// </summary>
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// The constructor of the home controller.
        /// </summary>
        /// <param name="logger">The list of the home controllers.</param>
        public HomeController(ILogger<HomeController> logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// The method that containst the index action.
        /// </summary>
        /// <returns>The result of the action.</returns>
        public IActionResult Index()
        {
            return this.View();
        }

        /// <summary>
        /// The method that containst the privacy action.
        /// </summary>
        /// <returns>The result of the action.</returns>
        public IActionResult Privacy()
        {
            return this.View();
        }

        /// <summary>
        /// The method that containst the error action.
        /// </summary>
        /// <returns>The result of the action.</returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return this.View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? this.HttpContext.TraceIdentifier });
        }
    }
}
