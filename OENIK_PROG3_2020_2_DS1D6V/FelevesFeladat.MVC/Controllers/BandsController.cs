﻿// <copyright file="BandsController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.MVC.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using AutoMapper;
    using FelevesFeladat.Logic;
    using FelevesFeladat.MVC.Models;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The band controller class.
    /// </summary>
    public class BandsController : Controller
    {
        private IEditDataStackLogic editDataStackLogic;
        private IGetEntityByDefinitionLogic getEntityByDefinitionLogic;
        private IModifyEntityLogic modifyEntityLogic;
        private IMapper mapper;
        private BandListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="BandsController"/> class.
        /// The constructor of the band controller class.
        /// </summary>
        /// <param name="editDataStackLogic">The editDataStackLogic.</param>
        /// <param name="getEntityByDefinitionLogic">The getEntityByDefinitionLogic.</param>
        /// <param name="modifyEntityLogic">The modifyEntityLogic.</param>
        /// <param name="mapper">The auto mapper.</param>
        public BandsController(IEditDataStackLogic editDataStackLogic, IGetEntityByDefinitionLogic getEntityByDefinitionLogic, IModifyEntityLogic modifyEntityLogic, IMapper mapper)
        {
            this.editDataStackLogic = editDataStackLogic;
            this.getEntityByDefinitionLogic = getEntityByDefinitionLogic;
            this.modifyEntityLogic = modifyEntityLogic;
            this.mapper = mapper;
            this.vm = new BandListViewModel();
            this.vm.EditedBand = new Models.Band();
            var bands = this.getEntityByDefinitionLogic.GetAllBands();
            this.vm.Bands = this.mapper.Map<IList<Data.Band>, List<Models.Band>>(bands);
        }

        /// <summary>
        /// The actionresult of the index view.
        /// </summary>
        /// <returns>The actionresult.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("BandsIndex", this.vm);
        }

        // Get: Bands/Details/5

        /// <summary>
        /// The actionresult of the details view.
        /// </summary>
        /// <param name="id">The id of the selected band.</param>
        /// <returns>The actionresult.</returns>
        public IActionResult Details(int id)
        {
            return this.View("BandsDetails", this.GetBandModel(id));
        }

        // GET: Bands/Remove/5

        /// <summary>
        /// The actionresult of the remove view.
        /// </summary>
        /// <param name="id">The id of the to be removed band.</param>
        /// <returns>The actionresult.</returns>
        public IActionResult Remove(int id)
        {
            this.TempData["editResult"] = "Delete OK";
            this.editDataStackLogic.RemoveBand(id.ToString(new CultureInfo("en")));
            return this.RedirectToAction(nameof(this.Index));
        }

        // GET: Bands/Edit/5

        /// <summary>
        /// The actionresult of the edit view.
        /// </summary>
        /// <param name="id">The id of the selected band for the edit.</param>
        /// <returns>The actionresult.</returns>
        public IActionResult Edit(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.vm.EditedBand = this.GetBandModel(id);
            return this.View("BandsIndex", this.vm);
        }

        // POST: Bands/Edit + 1car + editAction

        /// <summary>
        /// The actionresult of the edit view.
        /// </summary>
        /// <param name="band">The band model.</param>
        /// <param name="editAction">The action in a string.</param>
        /// <returns>The actionresult.</returns>
        [HttpPost]
        public IActionResult Edit(Models.Band band, string editAction)
        {
            if (this.ModelState.IsValid && band != null)
            {
                this.TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    try
                    {
                        this.editDataStackLogic.InsertBand(band.Name, band.Origin, band.Members, band.Genres, band.Formed.ToString(new CultureInfo("en")));
                    }
                    catch (ArgumentException ex)
                    {
                        this.TempData["editResult"] = "AddBand FAIL: " + ex.Message;
                    }
                }
                else
                {
                    this.modifyEntityLogic.UpdateBand(band.ID.ToString(new CultureInfo("en")), band.Name, band.Formed.ToString(new CultureInfo("en")), band.Genres, band.Members, band.Origin);
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.vm.EditedBand = band;
                return this.View("BandsIndex", this.vm);
            }
        }

        private Models.Band GetBandModel(int id)
        {
            Data.Band oneBand = this.getEntityByDefinitionLogic.GetBandById(id.ToString(new CultureInfo("en")));
            return this.mapper.Map<Data.Band, Models.Band>(oneBand);
        }
    }
}
