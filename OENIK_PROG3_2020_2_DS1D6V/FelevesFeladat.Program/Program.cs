﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using ConsoleTools;
    using FelevesFeladat.Data;
    using FelevesFeladat.Logic;
    using FelevesFeladat.Repository;

    /// <summary>
    /// Class that runs the application.
    /// </summary>
    internal class Program
    {
        private static void Main()
        {
        }

        /*
        Factory f = new Factory();
        f.Initialization();

                try
                {
                    var menu = new ConsoleMenu()
                        .Add(">> LIST ALL BANDS", () => ListAllBands(f.GetEntityByDefinitionLogic))
                        .Add(">> LIST ALL ALBUMS", () => ListAllAlbums(f.GetEntityByDefinitionLogic))
                        .Add(">> LIST ALL SONGS", () => ListAllSongs(f.GetEntityByDefinitionLogic))
                        .Add(">> LIST BAND BY ID", () => ListBandById(f.GetEntityByDefinitionLogic))
                        .Add(">> LIST ALBUM BY ID", () => ListAlbumById(f.GetEntityByDefinitionLogic))
                        .Add(">> LIST SONG BY ID", () => ListSongById(f.GetEntityByDefinitionLogic))
                        .Add(">> CHANGE BAND NAME", () => ChangeBandName(f.ModifyEntityLogic))
                        .Add(">> CHANGE ALBUM TITLE", () => ChangeAlbumName(f.ModifyEntityLogic))
                        .Add(">> CHANGE SONG NAME", () => ChangeSongName(f.ModifyEntityLogic))
                        .Add(">> INSERT BAND", () => InsertBand(f.EditDataStackLogic))
                        .Add(">> INSERT ALBUM", () => InsertAlbum(f.EditDataStackLogic))
                        .Add(">> INSERT SONG", () => InsertSong(f.EditDataStackLogic))
                        .Add(">> REMOVE BAND", () => RemoveBand(f.EditDataStackLogic))
                        .Add(">> REMOVE ALBUM", () => RemoveAlbum(f.EditDataStackLogic))
                        .Add(">> REMOVE SONG", () => RemoveSong(f.EditDataStackLogic))
                        .Add(">> SELECT THE MOST COMMON MONTH, WHEN ALBUMS WERE RELEASED", () => AlbumsByMonthsCount(f.GetEntityByDefinitionLogic))
                        .Add(">> SELECT THE LENGTH OF ALL THE ALBUMS FROM EACH COUNTRY ", () => BandsOriginPlaces(f.GetEntityByDefinitionLogic))
                        .Add(">> SELECT THE AVARAGE PLACE ON THE BILLBOARD", () => AlbumsSongsBillboardAvaragePlace(f.GetEntityByDefinitionLogic))
                        .Add(">> SELECT HOW MANY SONGS WITH MUSIC VIDEOS IN THE ALBUM", () => ContainsMusicVideo(f.GetEntityByDefinitionLogic))
                        .Add(">> [ASYNC] SELECT THE LENGTH OF ALL THE ALBUMS FROM EACH COUNTRY ", () => BandsOriginPlacesAsync(f.GetEntityByDefinitionLogic))
                        .Add(">> [ASYNC] SELECT THE AVARAGE PLACE ON THE BILLBOARD", () => AlbumsSongsBillboardAvaragePlaceAsync(f.GetEntityByDefinitionLogic))
                        .Add(">> [ASYNC] SELECT HOW MANY SONGS WITH MUSIC VIDEOS IN THE ALBUM", () => ContainsMusicVideoAsync(f.GetEntityByDefinitionLogic));

        menu.Show();
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (InvalidCastException e)
{
    Console.WriteLine(e.Message);
}
            }

            private static void ListAllBands(GetEntityByDefinitionLogic crudLogic)
{
    crudLogic.GetAllBands().ToList().ForEach(x => Console.WriteLine(x.Name + " | Id: " + x.ID));
    Console.ReadLine();
}

private static void ListAllAlbums(GetEntityByDefinitionLogic crudLogic)
{
    crudLogic.GetAllAlbums().ToList().ForEach(x => Console.WriteLine(x.Title + " | Id: " + x.ID));
    Console.ReadLine();
}

private static void ListAllSongs(GetEntityByDefinitionLogic crudLogic)
{
    crudLogic.GetAllSongs().ToList().ForEach(x => Console.WriteLine(x.Name + " | Id: " + x.ID));
    Console.ReadLine();
}

private static void ListBandById(GetEntityByDefinitionLogic crudLogic)
{
    Console.WriteLine("Which band do you want to select? (id)");
    string id = Console.ReadLine();
    Console.WriteLine("Selected band:");
    Console.WriteLine(crudLogic.GetBandById(id).Name);
    Console.ReadLine();
}

private static void ListAlbumById(GetEntityByDefinitionLogic crudLogic)
{
    Console.WriteLine("Which album do you want to select? (id)");
    string id = Console.ReadLine();
    Console.WriteLine("Selected album:");
    Console.WriteLine(crudLogic.GetAlbumById(id).Title);
    Console.ReadLine();
}

private static void ListSongById(GetEntityByDefinitionLogic crudLogic)
{
    Console.WriteLine("Which song do you want to select? (id)");
    string id = Console.ReadLine();
    Console.WriteLine("Selected song:");
    Console.WriteLine(crudLogic.GetSongById(id).Name);
    Console.ReadLine();
}

private static void ChangeBandName(ModifyEntityLogic crudLogic)
{
    Console.WriteLine("Which band's name do you want to change? (id)");
    string id = Console.ReadLine();
    Console.WriteLine("New name?");
    string newName = Console.ReadLine();
    crudLogic.ChangeBandName(id, newName);
    Console.WriteLine("Band's name changed");
    Console.ReadLine();
}

private static void ChangeAlbumName(ModifyEntityLogic crudLogic)
{
    Console.WriteLine("Which album's title do you want to change? (id)");
    string id = Console.ReadLine();
    Console.WriteLine("New title?");
    string newName = Console.ReadLine();
    crudLogic.ChangeAlbumTitle(id, newName);
    Console.WriteLine("Album's title changed");
    Console.ReadLine();
}

private static void ChangeSongName(ModifyEntityLogic crudLogic)
{
    Console.WriteLine("Which song's name do you want to change? (id)");
    string id = Console.ReadLine();
    Console.WriteLine("New name?");
    string newName = Console.ReadLine();
    crudLogic.ChangeSongName(id, newName);
    Console.WriteLine("Song's name changed");
    Console.ReadLine();
}

private static void InsertBand(EditDataStackLogic crudLogic)
{
    Console.WriteLine("Band's name:");
    string name = Console.ReadLine();
    Console.WriteLine("Band's origin:");
    string origin = Console.ReadLine();
    Console.WriteLine("Band's members:");
    string members = Console.ReadLine();
    Console.WriteLine("Band's genres:");
    string genres = Console.ReadLine();
    Console.WriteLine("Band's formed in:");
    string formed = Console.ReadLine();
    crudLogic.InsertBand(name, origin, members, genres, formed);
}

private static void InsertAlbum(EditDataStackLogic crudLogic)
{
    Console.WriteLine("Album's title:");
    string title = Console.ReadLine();
    Console.WriteLine("Album released:(MM.DD.YYYY)");
    string released = Console.ReadLine();
    Console.WriteLine("Album's language:");
    string language = Console.ReadLine();
    Console.WriteLine("Album's length:(FLOAT with '.')");
    string length = Console.ReadLine();
    Console.WriteLine("Album's label:");
    string label = Console.ReadLine();
    Console.WriteLine("Album's bandId:");
    string bandId = Console.ReadLine();
    crudLogic.InsertAlbum(title, released, language, length, label, bandId);
}

private static void InsertSong(EditDataStackLogic crudLogic)
{
    Console.WriteLine("Song's name:");
    string name = Console.ReadLine();
    Console.WriteLine("Song's musicVideo:");
    string musicVideo = Console.ReadLine();
    Console.WriteLine("Song's length:(FLOAT with '.')");
    string length = Console.ReadLine();
    Console.WriteLine("Song's guitarSolo:");
    string guitarSolo = Console.ReadLine();
    Console.WriteLine("Song's billboard:");
    string billboard = Console.ReadLine();
    Console.WriteLine("Song's albumId:");
    string albumId = Console.ReadLine();
    crudLogic.InsertSong(name, musicVideo, length, guitarSolo, billboard, albumId);
}

private static void RemoveBand(EditDataStackLogic crudLogic)
{
    Console.WriteLine("What band do you want to remove? (id):");

    crudLogic.RemoveBand(Console.ReadLine());
}

private static void RemoveAlbum(EditDataStackLogic crudLogic)
{
    Console.WriteLine("What album do you want to remove? (id):");

    crudLogic.RemoveAlbum(Console.ReadLine());
}

private static void RemoveSong(EditDataStackLogic crudLogic)
{
    Console.WriteLine("What song do you want to remove? (id):");

    crudLogic.RemoveSong(Console.ReadLine());
}

private static void BandsOriginPlaces(GetEntityByDefinitionLogic noncrudLogic)
{
    var query = noncrudLogic.BandsOriginPlaces();

    foreach (var item in query)
    {
        Console.WriteLine(item);
    }

    Console.ReadLine();
}

private static void AlbumsByMonthsCount(GetEntityByDefinitionLogic noncrudLogic)
{
    Console.WriteLine(noncrudLogic.AlbumsByMonthsCount());
    Console.ReadLine();
}

private static void AlbumsSongsBillboardAvaragePlace(GetEntityByDefinitionLogic noncrudLogic)
{
    Console.WriteLine("Which band?");
    string input = Console.ReadLine();
    var query = noncrudLogic.AlbumsSongsBillboardAvaragePlace(input);

    foreach (var item in query)
    {
        Console.WriteLine(item);
    }

    Console.ReadLine();
}

private static void ContainsMusicVideo(GetEntityByDefinitionLogic noncrudLogic)
{
    Console.WriteLine("Which Album?");
    string input = Console.ReadLine();
    var query = noncrudLogic.ContainsMusicVideo(input);

    foreach (var item in query)
    {
        Console.WriteLine(item);
    }

    Console.ReadLine();
}

private static void BandsOriginPlacesAsync(GetEntityByDefinitionLogic noncrudLogic)
{
    var query = noncrudLogic.BandsOriginPlacesAsync();
    var result = query.Result;

    foreach (var item in result)
    {
        Console.WriteLine(item);
    }

    Console.ReadLine();
}

private static void AlbumsSongsBillboardAvaragePlaceAsync(GetEntityByDefinitionLogic noncrudLogic)
{
    Console.WriteLine("Which band?");
    string input = Console.ReadLine();
    var query = noncrudLogic.AlbumsSongBillboardAvaragePlaceAsync(input);
    var result = query.Result;

    foreach (var item in result)
    {
        Console.WriteLine(item);
    }

    Console.ReadLine();
}

private static void ContainsMusicVideoAsync(GetEntityByDefinitionLogic noncrudLogic)
{
    Console.WriteLine("Which Album?");
    string input = Console.ReadLine();
    var query = noncrudLogic.ContainsMusicVideoAsync(input);
    var result = query.Result;

    foreach (var item in result)
    {
        Console.WriteLine(item);
    }

    Console.ReadLine();
}

*/
    }
}
