﻿// <copyright file="Factory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Program
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using FelevesFeladat.Data;
    using FelevesFeladat.Logic;
    using FelevesFeladat.Repository;

    /// <summary>
    /// Initializes objects for the program class.
    /// </summary>
    public class Factory
    {
        /// <summary>
        /// Gets or sets a class which contains the methods for database content editing.
        /// </summary>
        public EditDataStackLogic EditDataStackLogic { get; set; }

        /// <summary>
        /// Gets or sets logic class property, where the queries are.
        /// </summary>
        public GetEntityByDefinitionLogic GetEntityByDefinitionLogic { get; set; }

        /// <summary>
        /// Gets or sets the logic class property, where the entity modifier methods are located.
        /// </summary>
        public ModifyEntityLogic ModifyEntityLogic { get; set; }

        /// <summary>
        /// Initializes all the repository,logic and database classes.
        /// </summary>
        public void Initialization()
        {
            BandContext ctx = new BandContext();

            BandRepository bandRepo = new BandRepository(ctx);
            AlbumRepository albumRepo = new AlbumRepository(ctx);
            SongRepository songRepo = new SongRepository(ctx);

            this.EditDataStackLogic = new EditDataStackLogic(bandRepo, albumRepo, songRepo);
            this.GetEntityByDefinitionLogic = new GetEntityByDefinitionLogic(bandRepo, albumRepo, songRepo);
            this.ModifyEntityLogic = new ModifyEntityLogic(bandRepo, albumRepo, songRepo);
        }
    }
}
