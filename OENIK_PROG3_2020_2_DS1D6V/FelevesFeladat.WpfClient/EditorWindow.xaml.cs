﻿// <copyright file="EditorWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WpfClient
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for EditorWindow.xaml.
    /// </summary>
    public partial class EditorWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindow"/> class.
        /// The constructor of the editor window.
        /// </summary>
        public EditorWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindow"/> class.
        /// The constructo of the editor window with one parameter.
        /// </summary>
        /// <param name="band">The bandVM.</param>
        public EditorWindow(BandVM band)
            : this()
        {
            this.DataContext = band;
        }

        /// <summary>
        /// The event that occures when the inserting is accepted by a button click.
        /// </summary>
        /// <param name="sender">The object that sends the evenet.</param>
        /// <param name="e">Contains state information and event data.</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
