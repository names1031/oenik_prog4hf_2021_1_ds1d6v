﻿// <copyright file="BandVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FelevesFeladat.Data;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// The BandVM class that is used in the view layer.
    /// </summary>
    public class BandVM : ObservableObject
    {
        private readonly Band band;
        private int id;
        private string formed = "01.01.0001";
        private string name;
        private string origin;
        private string members;
        private string genres;

        /// <summary>
        /// Initializes a new instance of the <see cref="BandVM"/> class.
        /// The constructor of the BandVM.
        /// </summary>
        public BandVM()
        {
            this.band = new Band();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BandVM"/> class.
        /// The constructor that converts a Band entity to a BandVM entity.
        /// </summary>
        /// <param name="band">The band entity.</param>
        public BandVM(Band band)
        {
            /*Reflexióval:
                this.GetType().GetProperties().ToList().ForEach(
                    property => property.SetValue(this, property.GetValue(band)));*/

            this.band = band;
            this.formed = this.band.Formed.ToString(new CultureInfo("en"));
            this.genres = this.band.Genres;
            this.id = this.band.ID;
            this.members = this.band.Members;
            this.name = this.band.Name;
            this.origin = this.band.Origin;
        }

        /// <summary>
        /// Gets or sets the primary key of the band class.
        /// </summary>
        public int ID
        {
            get { return this.id; }
            set { this.Set(ref this.id, value); }
        }

        /// <summary>
        /// Gets or sets a band's name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets where the band was founded.
        /// </summary>
        public string Origin
        {
            get { return this.origin; }
            set { this.Set(ref this.origin, value); }
        }

        /// <summary>
        /// Gets or sets the members of the band.
        /// </summary>
        public string Members
        {
            get { return this.members; }
            set { this.Set(ref this.members, value); }
        }

        /// <summary>
        /// Gets or sets the bands genres.
        /// </summary>
        public string Genres
        {
            get { return this.genres; }
            set { this.Set(ref this.genres, value); }
        }

        /// <summary>
        /// Gets or sets when the band was formed.
        /// </summary>
        public string Formed
        {
            get { return this.formed; }
            set { this.Set(ref this.formed, value); }
        }

        /// <summary>
        /// The method that converts a BandVM entity to a Band entity.
        /// </summary>
        /// <returns>The Band entity.</returns>
        public Band ToBand()
        {
            this.band.Formed = DateTime.Parse(this.formed, new CultureInfo("en"));
            this.band.Genres = this.genres;
            this.band.ID = this.ID;
            this.band.Members = this.members;
            this.band.Name = this.name;
            this.band.Origin = this.origin;

            return this.band;
        }

        /// <summary>
        /// Copies a band vm.
        /// </summary>
        /// <param name="other">The other band vm.</param>
        public void CopyFrom(BandVM other)
        {
            if (other == null)
            {
                return;
            }

            this.ID = other.ID;
            this.Formed = other.Formed;
            this.Genres = other.Genres;
            this.Members = other.Members;
            this.Name = other.Name;
            this.Origin = other.Origin;
        }

        /// <summary>
        /// The override of the equals method.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>Whether this object equals the other object.</returns>
        public override bool Equals(object obj)
        {
            BandVM other = obj as BandVM;

            if (other is null)
            {
                return false;
            }

            return this.ID == other.ID;
        }

        /// <summary>
        /// The overide of the GetHashCode method.
        /// </summary>
        /// <returns>This object's hashCode.</returns>
        public override int GetHashCode()
        {
            return this.id.GetHashCode();
        }
    }
}