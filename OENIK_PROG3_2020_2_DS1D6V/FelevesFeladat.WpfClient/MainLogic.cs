﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// The main logic for the api wpf.
    /// </summary>
    public class MainLogic : IMainLogic
    {
        private string url = "http://localhost:9979/BandsApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <inheritdoc/>
        public void SendMessage(bool succes)
        {
            string msg = succes ? "Operation compledted successfully" : "Operation failed";
            Messenger.Default.Send(msg, "BandResult");
        }

        /// <inheritdoc/>
        public List<BandVM> ApiGetBands()
        {
            string json = this.client.GetStringAsync(this.url + "all").Result;
            var list = JsonSerializer.Deserialize<List<BandVM>>(json, this.jsonOptions);
            return list;
        }

        /// <inheritdoc/>
        public void ApiDelBand(BandVM band)
        {
            bool succes = true;

            if (band != null)
            {
                string json = this.client.GetStringAsync(this.url + "del/" + band.ID.ToString(new CultureInfo("en"))).Result;
                JsonDocument doc = JsonDocument.Parse(json);
            }

            this.SendMessage(succes);
        }

        /// <inheritdoc/>
        public void EditBand(BandVM band, Func<BandVM, bool> editoFunc)
        {
            BandVM clone = new BandVM();
            if (band != null)
            {
                clone.CopyFrom(band);
            }

            bool? succes = editoFunc?.Invoke(clone);

            if (succes == true)
            {
                if (band != null)
                {
                    succes = this.ApiEditBand(clone, true);
                }
                else
                {
                    succes = this.ApiEditBand(clone, false);
                }
            }

            this.SendMessage(succes == true);
        }

        /// <inheritdoc/>
        public bool ApiEditBand(BandVM band, bool isEditing)
        {
            if (band == null)
            {
                return false;
            }

            string myUrl = this.url + (isEditing ? "mod" : "add");

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("Id", band.ID.ToString(new CultureInfo("en")));
            }

            postData.Add("Name", band.Name);
            postData.Add("Origin", band.Origin);
            postData.Add("Formed", band.Formed);
            postData.Add("Genres", band.Genres);
            postData.Add("Members", band.Members);

            string json = this.client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;

            JsonDocument doc = JsonDocument.Parse(json);

            // return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            return true;
        }
    }
}
