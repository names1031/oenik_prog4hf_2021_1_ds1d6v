﻿// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using FelevesFeladat.Data;
    using FelevesFeladat.Repository;
    using Microsoft.EntityFrameworkCore;
    using Ninject;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        private IKernel kernel;

        /// <summary>
        /// Gets the mainVm.
        /// </summary>
        public MainVM MainVM
        {
            get
            {
                return this.kernel.Get<MainVM>();
            }
        }

        /// <summary>
        /// Occurs on startup.
        /// </summary>
        /// <param name="e">Contains the argument for the startup event.</param>
        protected override void OnStartup(StartupEventArgs e)
        {
            this.kernel = new StandardKernel();
            RegisterServices(this.kernel);
            base.OnStartup(e);
        }

        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IMainLogic>().To<MainLogic>();
            kernel.Bind<IBandRepository>().To<BandRepository>();
            kernel.Bind<IAlbumRepository>().To<AlbumRepository>();
            kernel.Bind<ISongRepository>().To<SongRepository>();
            kernel.Bind<DbContext>().To<BandContext>().InSingletonScope();
            kernel.Bind<DbContextOptions>().ToSelf().InSingletonScope();
        }
    }
}
