﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// The public mainVM class.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private IMainLogic logic;
        private BandVM selectedBand;
        private ObservableCollection<BandVM> bands;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// The constructor of the main vm.
        /// </summary>
        /// <param name="logic">The main logic.</param>
        public MainVM(IMainLogic logic)
        {
            this.logic = logic;
            this.LoadCMD = new RelayCommand(() =>
            {
                this.Bands = new ObservableCollection<BandVM>(this.logic.ApiGetBands());
            });

            this.DelCMD = new RelayCommand(() =>
            {
                this.logic.ApiDelBand(this.selectedBand);
            });

            this.AddCMD = new RelayCommand(() =>
            {
                this.logic.EditBand(null, this.EditorFunc);
            });

            this.ModCMD = new RelayCommand(() =>
            {
                this.logic.EditBand(this.selectedBand, this.EditorFunc);
            });
        }

        /// <summary>
        /// Gets or sets the bands.
        /// </summary>
        public ObservableCollection<BandVM> Bands
        {
            get { return this.bands; }
            set { this.Set(ref this.bands, value); }
        }

        /// <summary>
        /// Gets or sets the selected band.
        /// </summary>
        public BandVM SelectedBand
        {
            get { return this.selectedBand; }
            set { this.Set(ref this.selectedBand, value); }
        }

        /// <summary>
        /// Gets or sets the editor function.
        /// </summary>
        public Func<BandVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets or sets the add command.
        /// </summary>
        public ICommand AddCMD { get; set; }

        /// <summary>
        /// Gets or sets the delete command.
        /// </summary>
        public ICommand DelCMD { get; set; }

        /// <summary>
        /// Gets or sets the modify command.
        /// </summary>
        public ICommand ModCMD { get; set; }

        /// <summary>
        /// Gets or sets the load command.
        /// </summary>
        public ICommand LoadCMD { get; set; }
    }
}
