﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "Unnecessary warning.", Scope = "member", Target = "~M:FelevesFeladat.WPF.BandVM.GetHashCode~System.Int32")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1115:Parameter should follow comma", Justification = "Unnecessary warning.")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "Unnecessary warning.", Scope = "member", Target = "~M:FelevesFeladat.WpfClient.BandVM.GetHashCode~System.Int32")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "Unnecessary warning.", Scope = "member", Target = "~M:FelevesFeladat.WpfClient.MainLogic.ApiGetBands~System.Collections.Generic.List{FelevesFeladat.WpfClient.BandVM}")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "<Unnecessary warning.", Scope = "type", Target = "~T:FelevesFeladat.WpfClient.MainLogic")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "Unnecessary warning.", Scope = "member", Target = "~M:FelevesFeladat.WpfClient.MainLogic.ApiGetBands~System.Collections.Generic.List{FelevesFeladat.WpfClient.BandVM}")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1202:Elements should be ordered by access", Justification = "Unnecessary warning.", Scope = "member", Target = "~M:FelevesFeladat.WpfClient.MainLogic.ApiGetBands~System.Collections.Generic.List{FelevesFeladat.WpfClient.BandVM}")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "Unnecessary warning", Scope = "member", Target = "~M:FelevesFeladat.WpfClient.MainLogic.SendMessage(System.Boolean)")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "Unnecessary warning.", Scope = "member", Target = "~M:FelevesFeladat.WpfClient.MainLogic.ApiDelBand(FelevesFeladat.WpfClient.BandVM)")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "Unnecessary warning.", Scope = "member", Target = "~M:FelevesFeladat.WpfClient.MainLogic.ApiEditCar(FelevesFeladat.WpfClient.BandVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "Unnecessary warning.", Scope = "member", Target = "~M:FelevesFeladat.WpfClient.MainLogic.ApiEditCar(FelevesFeladat.WpfClient.BandVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "Unnecessary warning.", Scope = "member", Target = "~M:FelevesFeladat.WpfClient.MainLogic.ApiEditBand(FelevesFeladat.WpfClient.BandVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "Unnecessary warning.", Scope = "member", Target = "~M:FelevesFeladat.WpfClient.MainLogic.ApiEditBand(FelevesFeladat.WpfClient.BandVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Unnecessary warning.", Scope = "member", Target = "~P:FelevesFeladat.WpfClient.MainVM.Bands")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "Unnecessary warning.", Scope = "member", Target = "~M:FelevesFeladat.WpfClient.IMainLogic.ApiGetBands~System.Collections.Generic.List{FelevesFeladat.WpfClient.BandVM}")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "Unnecessary warning.", Scope = "type", Target = "~T:FelevesFeladat.WpfClient.App")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1014:MarkAssembliesWithClsCompliant", Justification = "Late bound")]