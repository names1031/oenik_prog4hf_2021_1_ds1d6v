﻿// <copyright file="IMainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The main logic interface.
    /// </summary>
    public interface IMainLogic
    {
        /// <summary>
        /// The send message method.
        /// </summary>
        /// <param name="succes">Whether the action is successfull or not.</param>
        void SendMessage(bool succes);

        /// <summary>
        /// Get all bands method.
        /// </summary>
        /// <returns>A bandVM list.</returns>
        List<BandVM> ApiGetBands();

        /// <summary>
        /// Delete band method.
        /// </summary>
        /// <param name="band">The bandVM.</param>
        void ApiDelBand(BandVM band);

        /// <summary>
        /// The edit band method.
        /// </summary>
        /// <param name="band">The bandVm.</param>
        /// <param name="editoFunc">The function that describes how to edit the band.</param>
        void EditBand(BandVM band, Func<BandVM, bool> editoFunc);

        /// <summary>
        /// The edit band method.
        /// </summary>
        /// <param name="band">The bandVM.</param>
        /// <param name="isEditing">Whether the band is in the progress of editing.</param>
        /// <returns>Whether the action succeded.</returns>
        bool ApiEditBand(BandVM band, bool isEditing);
    }
}
