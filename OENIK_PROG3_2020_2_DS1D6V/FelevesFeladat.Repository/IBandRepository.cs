﻿// <copyright file="IBandRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using FelevesFeladat.Data;

    /// <summary>
    /// The interface of the band repository.
    /// </summary>
    public interface IBandRepository : IRepository<Band>
    {
        /// <summary>
        /// Changes the name of the band.
        /// </summary>
        /// <param name="id">Variable which the search is based on.</param>
        /// <param name="newName">Variable which the old name will be replaced.</param>
        /// <param name="newFormed">The new formed date.</param>
        /// <param name="newGenres">The new genres.</param>
        /// <param name="newMembers">The new members.</param>
        /// <param name="newOrigin">The new origin of the band.</param>
        void UpdateBand(int id, string newName, DateTime newFormed, string newGenres, string newMembers, string newOrigin);
    }
}
