﻿// <copyright file="SongRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FelevesFeladat.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The repository of the song class.
    /// </summary>
    public class SongRepository : ElderRepository<Song>, ISongRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SongRepository"/> class.
        /// Initializes the song repository in the repository layer.
        /// </summary>
        /// <param name="ctx">Database class.</param>
        public SongRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public override Song GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.ID == id);
        }

        /// <inheritdoc/>
        public void UpdateSong(int id, string newName, bool newMusicVideo, double newLength, bool newGuitarSolo, int? newBillboard, int newAlbumID, Album album)
        {
            var song = this.GetOne(id);
            song.Name = newName;
            song.MusicVideo = newMusicVideo;
            song.Length = newLength;
            song.GuitarSolo = newGuitarSolo;
            song.Billboard = newBillboard;
            song.AlbumID = newAlbumID;
            song.Album = album;
            this.Ctx.SaveChanges();
        }
    }
}
