﻿// <copyright file="ElderRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FelevesFeladat.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Generic repository class.
    /// </summary>
    /// <typeparam name="T">Class type.</typeparam>
    public abstract class ElderRepository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ElderRepository{T}"/> class.
        /// Makes a connection with the data layer.
        /// </summary>
        /// <param name="ctx">The database context from the data layer.</param>
        protected ElderRepository(DbContext ctx)
        {
            this.Ctx = ctx;
        }

        /// <summary>
        /// Gets or sets the database context.
        /// </summary>
        public DbContext Ctx { get; set; }

        /// <inheritdoc/>
        public IQueryable<T> GetAll()
        {
            return this.Ctx.Set<T>();
        }

        /// <inheritdoc/>
        public abstract T GetOne(int id);

        /// <inheritdoc/>
        public void Insert(T entity)
        {
            this.Ctx.Set<T>().Add(entity);
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void Remove(T entity)
        {
            this.Ctx.Set<T>().Remove(entity);
            this.Ctx.SaveChanges();
        }
    }
}
