﻿// <copyright file="BandRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using FelevesFeladat.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The repository of the band class.
    /// </summary>
    public class BandRepository : ElderRepository<Band>, IBandRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BandRepository"/> class.
        /// Initializes the band repository in the repository layer.
        /// </summary>
        /// <param name="ctx">Database class.</param>
        public BandRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public void UpdateBand(int id, string newName, DateTime newFormed, string newGenres, string newMembers, string newOrigin)
        {
            var band = this.GetOne(id);
            band.Name = newName;
            band.Formed = newFormed;
            band.Genres = newGenres;
            band.Members = newMembers;
            band.Origin = newOrigin;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override Band GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.ID == id);
        }
    }
}
