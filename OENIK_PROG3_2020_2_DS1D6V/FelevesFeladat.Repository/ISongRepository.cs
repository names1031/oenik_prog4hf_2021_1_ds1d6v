﻿// <copyright file="ISongRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using FelevesFeladat.Data;

    /// <summary>
    /// The interface of the song repository.
    /// </summary>
    public interface ISongRepository : IRepository<Song>
    {
        /// <summary>
        /// Changes the name of the song.
        /// </summary>
        /// <param name="id">Variable which the search is based on.</param>
        /// <param name="newName">Variable which the old name will be replaced.</param>
        /// <param name="newMusicVideo">Variable which the old musicvideo bool will be replaced.</param>
        /// <param name="newLength">Variable which the old length will be replaced.</param>
        /// <param name="newGuitarSolo">Variable which the old guitarsolo bool will be replaced.</param>
        /// <param name="newBillboard">Variable which the old billboard will be replaced.</param>
        /// <param name="newAlbumID">Variable which the old album id will be replaced.</param>
        /// <param name="album">Variable which the old album will be replaced.</param>
        void UpdateSong(int id, string newName, bool newMusicVideo, double newLength, bool newGuitarSolo, int? newBillboard, int newAlbumID, Album album);
    }
}
