﻿// <copyright file="IAlbumRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using FelevesFeladat.Data;

    /// <summary>
    /// The interface of the album repository.
    /// </summary>
    public interface IAlbumRepository : IRepository<Album>
    {
        /// <summary>
        /// Changes the title of the album.
        /// </summary>
        /// <param name="id">Variable which the search is based on.</param>
        /// <param name="newTitle">Variable which the old name will be replaced.</param>
        /// <param name="newReleased">Variable which the old released var will be replaced.</param>
        /// <param name="newLanguage">Variable which the old language will be replaced.</param>
        /// <param name="newLength">Variable which the old length will be replaced.</param>
        /// <param name="newLabel">Variable which the old label will be replaced.</param>
        /// <param name="newBandID">Variable which the old band id will be replaced.</param>
        /// <param name="band">Variable which the old band will be replaced.</param>
        void UpdateAlbum(int id, string newTitle, DateTime newReleased, string newLanguage, double newLength, string newLabel, int newBandID, Band band);
    }
}
