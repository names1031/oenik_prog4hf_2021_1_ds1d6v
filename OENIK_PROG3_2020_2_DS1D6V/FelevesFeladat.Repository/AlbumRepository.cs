﻿// <copyright file="AlbumRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FelevesFeladat.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The repository of the album class.
    /// </summary>
    public class AlbumRepository : ElderRepository<Album>, IAlbumRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AlbumRepository"/> class.
        /// Initializes the album repository in the repository layer.
        /// </summary>
        /// <param name="ctx">Database class.</param>
        public AlbumRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public void UpdateAlbum(int id, string newTitle, DateTime newReleased, string newLanguage, double newLength, string newLabel, int newBandID, Band band)
        {
            var album = this.GetOne(id);
            album.Title = newTitle;
            album.Released = newReleased;
            album.Language = newLanguage;
            album.Length = newLength;
            album.Label = newLabel;
            album.BandID = newBandID;
            album.Band = band;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override Album GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.ID == id);
        }
    }
}
