﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1602:Enumeration items should be documented", Justification = "It makes the code unnecessarily chaotic.", Scope = "type", Target = "~T:FelevesFeladat.Logic.EnumMonth")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "It makes the code unnecessarily chaotic.", Scope = "member", Target = "~M:FelevesFeladat.Logic.BandsOriginAlbumLength.GetHashCode~System.Int32")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1014:MarkAssembliesWithClsCompliant", Justification = "Late bound")]
