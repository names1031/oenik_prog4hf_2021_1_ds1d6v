﻿// <copyright file="IEditDataStackLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Interface for the "EditDataStackLogic".
    /// </summary>
    public interface IEditDataStackLogic
    {
        /// <summary>
        /// Inserts a band object into the database.
        /// </summary>
        /// <param name="name">The band's name.</param>
        /// <param name="origin">The place where the band was formed.</param>
        /// <param name="members">The members of the band.</param>
        /// <param name="genres">The genre of the band.</param>
        /// <param name="formed">The date time, when the band was formed.</param>
        void InsertBand(string name, string origin, string members, string genres, string formed);

        /// <summary>
        /// Insert an album object in to the database.
        /// </summary>
        /// <param name="title">The title of the song.</param>
        /// <param name="released">The datetime, when the song was released.</param>
        /// <param name="language">The album's language.</param>
        /// <param name="length">The album's length.</param>
        /// <param name="label">The album's label.</param>
        /// <param name="bandId">The band's id, that released the album.</param>
        void InsertAlbum(string title, string released, string language, string length, string label, string bandId);

        /// <summary>
        /// Inserts song object in to the database.
        /// </summary>
        /// <param name="name">The name of the song.</param>
        /// <param name="musicVideo">Whether the song has a music video.</param>
        /// <param name="length">The length of the song.</param>
        /// <param name="guitarSolo">whether the song contains a guitar solo.</param>
        /// <param name="billboard">The place on the billboard.</param>
        /// <param name="albumId">The album's ID, that contains the song.</param>
        void InsertSong(string name, string musicVideo, string length, string guitarSolo, string billboard, string albumId);

        /// <summary>
        /// Removes a band obect by id from the database.
        /// </summary>
        /// <param name="id">The id, which the removal is based on.</param>
        void RemoveBand(string id);

        /// <summary>
        /// Removes an album obect by id from the database.
        /// </summary>
        /// <param name="id">The id, which the removal is based on.</param>
        void RemoveAlbum(string id);

        /// <summary>
        /// Removes a song obect by id from the database.
        /// </summary>
        /// <param name="id">The id, which the removal is based on.</param>
        void RemoveSong(string id);
    }
}
