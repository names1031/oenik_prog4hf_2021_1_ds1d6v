﻿// <copyright file="IModifyEntityLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Interface for the "ModifyEntityLogic" class.
    /// </summary>
    public interface IModifyEntityLogic
    {
        /// <summary>
        /// Changes the band's datas.
        /// </summary>
        /// <param name="id">The band's id, which band will be changed.</param>
        /// <param name="newName">The new name.</param>
        /// <param name="newFormed">The new formed datetime.</param>
        /// <param name="newGenres">The new genres variable.</param>
        /// <param name="newMembers">The new members.</param>
        /// <param name="newOrigin">The new origin.</param>
        void UpdateBand(string id, string newName, string newFormed, string newGenres, string newMembers, string newOrigin);

        /// <summary>
        /// Changes the album's datas.
        /// </summary>
        /// <param name="id">The Album's id, which title will be changed.</param>
        /// <param name="newTitle">The new title.</param>
        /// <param name="newReleased">The new released.</param>
        /// <param name="newLanguage">The new language.</param>
        /// <param name="newLength">The new length.</param>
        /// <param name="newLabel">The new label.</param>
        /// <param name="newBandID">The new bandID.</param>
        void UpdateAlbum(string id, string newTitle, string newReleased, string newLanguage, string newLength, string newLabel, string newBandID);

        /// <summary>
        /// Changes the song's datas.
        /// </summary>
        /// <param name="id">The Song's id, which name will be changed.</param>
        /// <param name="newName">The new name.</param>
        /// <param name="newMusicVideo">The new music video.</param>
        /// <param name="newLength">The new length.</param>
        /// <param name="newGuitarSolo">The new guitar solo.</param>
        /// <param name="newBillboard">The new billboard.</param>
        /// <param name="newAlbumID">The new album id.</param>
        void UpdateSong(string id, string newName, bool newMusicVideo, string newLength, bool newGuitarSolo, string newBillboard, string newAlbumID);
    }
}
