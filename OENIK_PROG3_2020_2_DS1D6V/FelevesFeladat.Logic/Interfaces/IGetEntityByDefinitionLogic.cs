﻿// <copyright file="IGetEntityByDefinitionLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using FelevesFeladat.Data;

    /// <summary>
    /// Interface for the "GetEntityByDefinitionLogic" class.
    /// </summary>
    public interface IGetEntityByDefinitionLogic
    {
        /// <summary>
        /// Returns all the bands in the database.
        /// </summary>
        /// <returns>A list with all the bands.</returns>
        IList<Band> GetAllBands();

        /// <summary>
        /// Returns all the albums in the database.
        /// </summary>
        /// <returns>A list with all the albums.</returns>
        IList<Album> GetAllAlbums();

        /// <summary>
        /// Returns all the songs in the database.
        /// </summary>
        /// <returns>A list with all the songs.</returns>
        IList<Song> GetAllSongs();

        /// <summary>
        /// Returns a album by id.
        /// </summary>
        /// <param name="id">The Id,which the search is based on.</param>
        /// <returns>An album object.</returns>
        Band GetBandById(string id);

        /// <summary>
        /// Returns an album by id.
        /// </summary>
        /// <param name="id">The Id,which the search is based on.</param>
        /// <returns>An album object.</returns>
        Album GetAlbumById(string id);

        /// <summary>
        /// Returns a song by id.
        /// </summary>
        /// <param name="id">The Id,which the search is based on.</param>
        /// <returns>A song object.</returns>
        Song GetSongById(string id);

        /// <summary>
        /// Query, that returns the most common month, when an album was released.
        /// </summary>
        /// <returns>The most common month for album releasing.</returns>
        MostCommonMonth AlbumsByMonthsCount();

        /// <summary>
        /// Query, that counts the album length grouped by country.
        /// </summary>
        /// <returns>The countries album length in a list.</returns>
        IList<BandsOriginAlbumLength> BandsOriginPlaces();

        /// <summary>
        /// Query, that returns the avarage billborad placement of the band's albums.
        /// </summary>
        /// <param name="band">The band's name.</param>
        /// <returns>The avarage billboard placement per album.</returns>
        IList<BillboardAvargaeResult> AlbumsSongsBillboardAvaragePlace(string band);

        /// <summary>
        /// Query, that counts how many songs with music videos there are in the specified album.
        /// </summary>
        /// <param name="album">The album's title.</param>
        /// <returns>How many songs with and without music videos.</returns>
        IList<SongsWithVideos> ContainsMusicVideo(string album);

        /// <summary>
        /// Query, that counts the album length grouped by country.
        /// </summary>
        /// <returns>Task that contains the countries album length in a list.</returns>
        Task<IList<BandsOriginAlbumLength>> BandsOriginPlacesAsync();

        /// <summary>
        /// Query, that returns the avarage billborad placement of the band's albums.
        /// </summary>
        /// <param name="band">Task that contains the band's name.</param>
        /// <returns>The avarage billboard placement per album.</returns>
        Task<IList<BillboardAvargaeResult>> AlbumsSongBillboardAvaragePlaceAsync(string band);

        /// <summary>
        /// Query, that counts how many songs with music videos there are in the specified album.
        /// </summary>
        /// <param name="album">Task that contains the album's title.</param>
        /// <returns>How many songs with and without music videos.</returns>
        Task<IList<SongsWithVideos>> ContainsMusicVideoAsync(string album);
    }
}
