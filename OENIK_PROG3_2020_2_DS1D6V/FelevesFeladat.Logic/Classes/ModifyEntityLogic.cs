﻿// <copyright file="ModifyEntityLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using FelevesFeladat.Repository;

    /// <summary>
    /// Contains the methods for changing the entities contents.
    /// </summary>
    public class ModifyEntityLogic : IModifyEntityLogic
    {
        private readonly IBandRepository bandRepo;

        private readonly IAlbumRepository albumRepo;

        private readonly ISongRepository songRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModifyEntityLogic"/> class.
        /// Sets the fields on implementation by the parameters.
        /// </summary>
        /// <param name="bandRepo">The repository interface for the band entity.</param>
        /// <param name="albumRepo">The repository interface for the album entity.</param>
        /// <param name="songRepo">The repository interface for the song entity.</param>
        public ModifyEntityLogic(IBandRepository bandRepo, IAlbumRepository albumRepo, ISongRepository songRepo)
        {
            this.bandRepo = bandRepo;
            this.albumRepo = albumRepo;
            this.songRepo = songRepo;
        }

        /// <inheritdoc/>
        public void UpdateBand(string id, string newName, string newFormed, string newGenres, string newMembers, string newOrigin)
        {
            int integerBandId = int.Parse(id, new CultureInfo("en"));

            DateTime dateFormed;

            if (!DateTime.TryParse(newFormed, out dateFormed))
            {
                dateFormed = DateTime.Parse("01.01.0001", new CultureInfo("en"));
            }

            this.bandRepo.UpdateBand(integerBandId, newName, dateFormed, newGenres, newMembers, newOrigin);
        }

        /// <inheritdoc/>
        public void UpdateAlbum(string id, string newTitle, string newReleased, string newLanguage, string newLength, string newLabel, string newBandID)
        {
            DateTime dateFormed;

            if (!DateTime.TryParse(newReleased, out dateFormed))
            {
                dateFormed = DateTime.Parse("01.01.001", new CultureInfo("en"));
            }

            double lengthFormed;

            if (!double.TryParse(newLength, out lengthFormed))
            {
                lengthFormed = double.Parse("0.0", new CultureInfo("en"));
            }

            int formedBandId;

            if (!int.TryParse(newBandID, out formedBandId))
            {
                formedBandId = int.Parse("1", new CultureInfo("en"));
            }

            int integerId = int.Parse(id, new CultureInfo("en"));

            this.albumRepo.UpdateAlbum(integerId, newTitle, dateFormed, newLanguage, lengthFormed, newLabel, formedBandId, this.bandRepo.GetOne(formedBandId));
        }

        /// <inheritdoc/>
        public void UpdateSong(string id, string newName, bool newMusicVideo, string newLength, bool newGuitarSolo, string newBillboard, string newAlbumID)
        {
            int integerSongId = int.Parse(id, new CultureInfo("en"));

            double formedLength;

            if (!double.TryParse(newLength, out formedLength))
            {
                formedLength = 0.0;
            }

            int formedBillboard;
            int? nullableBillboard;

            if (newBillboard?.Length == 0)
            {
                nullableBillboard = null;
            }
            else if (!int.TryParse(newBillboard, out formedBillboard))
            {
                nullableBillboard = 0;
            }
            else
            {
                nullableBillboard = formedBillboard;
            }

            int formedAlbumId = int.Parse(newAlbumID, new CultureInfo("en"));

            this.songRepo.UpdateSong(integerSongId, newName, newMusicVideo, formedLength, newGuitarSolo, nullableBillboard, formedAlbumId, this.albumRepo.GetOne(formedAlbumId));
        }
    }
}
