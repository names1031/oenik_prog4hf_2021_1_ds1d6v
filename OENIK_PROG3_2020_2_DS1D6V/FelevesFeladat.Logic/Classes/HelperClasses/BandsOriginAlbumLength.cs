﻿// <copyright file="BandsOriginAlbumLength.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Helper class for the "SelectedAlbumsGuitarSolos" method.
    /// </summary>
    public class BandsOriginAlbumLength
    {
        /// <summary>
        /// Gets or sets the band's name.
        /// </summary>
        public double SumAlbumLength { get; set; }

        /// <summary>
        /// Gets or sets the band's origin.
        /// </summary>
        public string BandsOrigin { get; set; }

        /// <summary>
        /// Overrides the ToString method for this class.
        /// </summary>
        /// <returns>Returns a string segment with the datas.</returns>
        public override string ToString()
        {
            return "From: " + this.BandsOrigin + ", every band's album's cumulated length is: " + this.SumAlbumLength;
        }

        /// <summary>
        /// Overrides the Equals method for the class.
        /// </summary>
        /// <param name="obj">The other obj that the comparison is based on.</param>
        /// <returns>If this object is equal to the other object or not.</returns>
        public override bool Equals(object obj)
        {
            if (obj is BandsOriginAlbumLength)
            {
                BandsOriginAlbumLength gsc = obj as BandsOriginAlbumLength;
                return this.SumAlbumLength == gsc.SumAlbumLength && this.BandsOrigin == gsc.BandsOrigin;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Overrides the GetHashCode method for the class.
        /// </summary>
        /// <returns>The hashcode of the class.</returns>
        public override int GetHashCode()
        {
            return this.SumAlbumLength.GetHashCode();
        }
    }
}
