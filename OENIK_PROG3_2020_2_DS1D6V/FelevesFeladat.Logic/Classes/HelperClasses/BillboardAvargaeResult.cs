﻿// <copyright file="BillboardAvargaeResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Helper class for the "BandsAlbumsSongsBillboardAvaragePlace" method.
    /// </summary>
    public class BillboardAvargaeResult
    {
        /// <summary>
        /// Gets or sets the Album's name the query is based on.
        /// </summary>
        public string AlbumTitle { get; set; }

        /// <summary>
        /// Gets or sets the avarage places of the album's songs.
        /// </summary>
        public double AvaragePlace { get; set; }

        /// <summary>
        /// Overrides the ToString method for the class.
        /// </summary>
        /// <returns>Returns a string segment with the datas.</returns>
        public override string ToString()
        {
            return "Album:  " + this.AlbumTitle + "\t\tAvarage place on the billboard:  " + this.AvaragePlace;
        }

        /// <summary>
        /// Overrides the Equals method for the class.
        /// </summary>
        /// <param name="obj">The other obj that the comparison is based on.</param>
        /// <returns>If this object is equal to the other object or not.</returns>
        public override bool Equals(object obj)
        {
            if (obj is BillboardAvargaeResult)
            {
                BillboardAvargaeResult bar = obj as BillboardAvargaeResult;

                return this.AlbumTitle == bar.AlbumTitle && this.AvaragePlace == bar.AvaragePlace;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Overrides the GetHashCode method for the class.
        /// </summary>
        /// <returns>The hashcode of the class.</returns>
        public override int GetHashCode()
        {
            return (int)this.AvaragePlace.GetHashCode();
        }
    }
}
