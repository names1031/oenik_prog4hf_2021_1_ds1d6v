﻿// <copyright file="EnumMonth.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Enum that contains the months.
    /// </summary>
    public enum EnumMonth
    {
        /// <summary>
        /// It is used in the ToString to write None.
        /// </summary>
        None,

        /// <summary>
        /// It is used in the ToString to write January.
        /// </summary>
        January = 1,

        /// <summary>
        /// It is used in the ToString to write February.
        /// </summary>
        Februray = 2,

        /// <summary>
        /// It is used in the ToString to write March.
        /// </summary>
        March = 3,

        /// <summary>
        /// It is used in the ToString to write April.
        /// </summary>
        April = 4,

        /// <summary>
        /// It is used in the ToString to write May.
        /// </summary>
        May = 5,

        /// <summary>
        /// It is used in the ToString to write Juny.
        /// </summary>
        Juny = 6,

        /// <summary>
        /// It is used in the ToString to write July.
        /// </summary>
        July = 7,

        /// <summary>
        /// It is used in the ToString to write August.
        /// </summary>
        August = 8,

        /// <summary>
        /// It is used in the ToString to write September.
        /// </summary>
        September = 9,

        /// <summary>
        /// It is used in the ToString to write October.
        /// </summary>
        October = 10,

        /// <summary>
        /// It is used in the ToString to write November.
        /// </summary>
        November = 11,

        /// <summary>
        /// It is used in the ToString to write December.
        /// </summary>
        December = 12,
    }
}
