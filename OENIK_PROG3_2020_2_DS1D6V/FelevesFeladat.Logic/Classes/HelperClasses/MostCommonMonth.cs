﻿// <copyright file="MostCommonMonth.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Helper class for the "AlbumsByMonthsCount" method.
    /// </summary>
    public class MostCommonMonth
    {
        /// <summary>
        /// Gets or sets the month, that is the most common.
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// Overrides the ToString method for this class.
        /// </summary>
        /// <returns>Returns a string segment with the datas.</returns>
        public override string ToString()
        {
            return "Most common month, when albums were released: " + (EnumMonth)this.Month;
        }

        /// <summary>
        /// Overrides the Equals method for the class.
        /// </summary>
        /// <param name="obj">The other obj that the comparison is based on.</param>
        /// <returns>If this object is equal to the other object or not.</returns>
        public override bool Equals(object obj)
        {
            if (obj is MostCommonMonth)
            {
                MostCommonMonth mcm = obj as MostCommonMonth;

                return this.Month == mcm.Month;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Overrides the GetHashCode method for the class.
        /// </summary>
        /// <returns>The hashcode of the class.</returns>
        public override int GetHashCode()
        {
            return this.Month.GetHashCode();
        }
    }
}
