﻿// <copyright file="SongsWithVideos.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Helper class for the "ContainsGuitarSolo" method.
    /// </summary>
    public class SongsWithVideos
    {
        /// <summary>
        /// Gets or sets a value indicating whether the song contains a guitar solo.
        /// </summary>
        public bool ContainsVideo { get; set; }

        /// <summary>
        /// Gets or sets how many songs in the album with a guitar solo.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Overrides the ToString method for this class.
        /// </summary>
        /// <returns>Returns a string segment with the datas.</returns>
        public override string ToString()
        {
            if (this.Count > 1)
            {
                if (this.ContainsVideo)
                {
                    return "The album has: " + this.Count + " songs with music video.";
                }
                else
                {
                    return "The album has: " + this.Count + " songs without music video.";
                }
            }
            else
            {
                if (this.ContainsVideo)
                {
                    return "The album has: " + this.Count + " song with music video.";
                }
                else
                {
                    return "The album has: " + this.Count + " song without music video.";
                }
            }
        }

        /// <summary>
        /// Overrides the Equals method for the class.
        /// </summary>
        /// <param name="obj">The other obj that the comparison is based on.</param>
        /// <returns>If this object is equal to the other object or not.</returns>
        public override bool Equals(object obj)
        {
            if (obj is SongsWithVideos)
            {
                SongsWithVideos sws = obj as SongsWithVideos;

                return sws.Count == this.Count && sws.ContainsVideo == this.ContainsVideo;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Overrides the GetHashCode method for the class.
        /// </summary>
        /// <returns>The hashcode of the class.</returns>
        public override int GetHashCode()
        {
            return this.ContainsVideo.GetHashCode();
        }
    }
}
