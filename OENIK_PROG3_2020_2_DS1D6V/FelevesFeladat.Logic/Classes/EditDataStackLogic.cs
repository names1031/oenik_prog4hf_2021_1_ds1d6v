﻿// <copyright file="EditDataStackLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using FelevesFeladat.Data;
    using FelevesFeladat.Repository;

    /// <summary>
    /// The class which contains the methods for database content editing.
    /// </summary>
    public class EditDataStackLogic : IEditDataStackLogic
    {
        private readonly IBandRepository bandRepo;

        private readonly IAlbumRepository albumRepo;

        private readonly ISongRepository songRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditDataStackLogic"/> class.
        /// Sets the fields on implementation by the parameters.
        /// </summary>
        /// <param name="bandRepo">The repository interface for the band entity.</param>
        /// <param name="albumRepo">The repository interface for the album entity.</param>
        /// <param name="songRepo">The repository interface for the song entity.</param>
        public EditDataStackLogic(IBandRepository bandRepo, IAlbumRepository albumRepo, ISongRepository songRepo)
        {
            this.bandRepo = bandRepo;
            this.albumRepo = albumRepo;
            this.songRepo = songRepo;
        }

        /// <inheritdoc/>
        public void InsertBand(string name, string origin, string members, string genres, string formed)
        {
            DateTime dateTimeFormed;
            if (!DateTime.TryParse(formed, out dateTimeFormed))
            {
                dateTimeFormed = DateTime.Parse("01.01.0001", new CultureInfo("en"));
            }

            Band band = new Band();
            band.Name = name;
            band.Origin = origin;
            band.Members = members;
            band.Genres = genres;
            band.Formed = dateTimeFormed;

            this.bandRepo.Insert(band);
        }

        /// <inheritdoc/>
        public void InsertAlbum(string title, string released, string language, string length, string label, string bandId)
        {
            DateTime dateTimeReleased;
            if (!DateTime.TryParse(released, out dateTimeReleased))
            {
                dateTimeReleased = DateTime.Parse("01.01.0001", new CultureInfo("en"));
            }

            double doubleLength;
            if (!double.TryParse(length, NumberStyles.Float | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.AllowExponent, CultureInfo.CreateSpecificCulture("en-GB"), out doubleLength))
            {
                doubleLength = 0.0;
            }

            int integerBandId;
            if (!int.TryParse(bandId, out integerBandId))
            {
                integerBandId = 1;
            }

            Album album = new Album();
            album.Title = title;
            album.Released = dateTimeReleased; // DateTime.Parse(released, new CultureInfo("en"));
            album.Language = language;
            album.Length = doubleLength; // double.Parse(length, NumberStyles.Float | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.AllowExponent, CultureInfo.CreateSpecificCulture("en-GB"));
            album.Label = label;
            album.BandID = integerBandId; // int.Parse(bandId, CultureInfo.CreateSpecificCulture("en-GB"));
            album.Band = this.bandRepo.GetOne(integerBandId);

            this.albumRepo.Insert(album);
        }

        /// <inheritdoc/>
        public void InsertSong(string name, string musicVideo, string length, string guitarSolo, string billboard, string albumId)
        {
            double doubleLength;
            if (!double.TryParse(length, NumberStyles.Float | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.AllowExponent, CultureInfo.CreateSpecificCulture("en-GB"), out doubleLength))
            {
                doubleLength = 0.0;
            }

            int integerBillboard;
            if (!int.TryParse(billboard, out integerBillboard))
            {
                integerBillboard = 0;
            }

            int integerAlbumId;
            if (!int.TryParse(albumId, out integerAlbumId))
            {
                integerAlbumId = 1;
            }

            Song song = new Song();
            song.Name = name;
            song.MusicVideo = bool.Parse(musicVideo);
            song.Length = doubleLength; // double.Parse(length, NumberStyles.Float | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.AllowExponent, CultureInfo.CreateSpecificCulture("en-GB"));
            song.GuitarSolo = bool.Parse(guitarSolo);
            song.Billboard = integerBillboard; // int.Parse(billboard, CultureInfo.CreateSpecificCulture("en-GB"));
            song.AlbumID = integerAlbumId; // int.Parse(albumId, CultureInfo.CreateSpecificCulture("en-GB"));
            song.Album = this.albumRepo.GetOne(integerAlbumId);

            this.songRepo.Insert(song);
        }

        /// <inheritdoc/>
        public void RemoveBand(string id)
        {
            int integerBandId = int.Parse(id, new CultureInfo("en"));

            this.bandRepo.Remove(this.bandRepo.GetOne(integerBandId));
        }

        /// <inheritdoc/>
        public void RemoveAlbum(string id)
        {
            int integerAlbumId = int.Parse(id, new CultureInfo("en"));

            this.albumRepo.Remove(this.albumRepo.GetOne(integerAlbumId));
        }

        /// <inheritdoc/>
        public void RemoveSong(string id)
        {
            int integerSongId = int.Parse(id, new CultureInfo("en"));

            this.songRepo.Remove(this.songRepo.GetOne(integerSongId));
        }
    }
}
