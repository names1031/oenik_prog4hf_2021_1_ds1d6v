﻿// <copyright file="GetEntityByDefinitionLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FelevesFeladat.Data;
    using FelevesFeladat.Repository;

    /// <summary>
    /// Contains the methods, which return information about one or more entities by some definition.
    /// </summary>
    public class GetEntityByDefinitionLogic : IGetEntityByDefinitionLogic
    {
        private readonly IBandRepository bandRepo;

        private readonly IAlbumRepository albumRepo;

        private readonly ISongRepository songRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetEntityByDefinitionLogic"/> class.
        /// Sets the fields on implementation by the parameters.
        /// </summary>
        /// <param name="bandRepo">The repository interface for the band entity.</param>
        /// <param name="albumRepo">The repository interface for the album entity.</param>
        /// <param name="songRepo">The repository interface for the song entity.</param>
        public GetEntityByDefinitionLogic(IBandRepository bandRepo, IAlbumRepository albumRepo, ISongRepository songRepo)
        {
            this.bandRepo = bandRepo;
            this.albumRepo = albumRepo;
            this.songRepo = songRepo;
        }

        /// <inheritdoc/>
        public IList<Band> GetAllBands()
        {
            return this.bandRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public IList<Album> GetAllAlbums()
        {
            return this.albumRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public IList<Song> GetAllSongs()
        {
            return this.songRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public Band GetBandById(string id)
        {
            int integerBandId;
            if (!int.TryParse(id, out integerBandId))
            {
                throw new InvalidCastException("Input error");
            }

            return this.bandRepo.GetOne(integerBandId);
        }

        /// <inheritdoc/>
        public Album GetAlbumById(string id)
        {
            int integerAlbumId;
            if (!int.TryParse(id, out integerAlbumId))
            {
                throw new InvalidCastException("Input error");
            }

            return this.albumRepo.GetOne(integerAlbumId);
        }

        /// <inheritdoc/>
        public Song GetSongById(string id)
        {
            int integerSongId;
            if (!int.TryParse(id, out integerSongId))
            {
                throw new InvalidCastException("Input error");
            }

            // else
            // {
            //    if (integerSongId < this.songRepo.GetAll().OrderBy(x => x.ID).First().ID || integerSongId > this.songRepo.GetAll().OrderBy(x => x.ID).Last().ID)
            //    {
            //        throw new ArgumentException("There is no song with this ID");
            //    }
            // }
            return this.songRepo.GetOne(integerSongId);
        }

        /// <inheritdoc/>
        public IList<BandsOriginAlbumLength> BandsOriginPlaces()
        {
            var q = from album in this.albumRepo.GetAll()
                    group album by album.Band.Origin into g
                    select new BandsOriginAlbumLength()
                    {
                        BandsOrigin = g.Key,
                        SumAlbumLength = g.Sum(x => x.Length),
                    };
            return q.ToList();
        }

        /// <inheritdoc/>
        public MostCommonMonth AlbumsByMonthsCount()
        {
            var q = from album in this.albumRepo.GetAll()
                    group album by album.Released.Month into g
                    orderby g.Count() descending
                    select new MostCommonMonth()
                    {
                        Month = g.Key,
                    };

            return q.FirstOrDefault();
        }

        /// <inheritdoc/>
        public IList<BillboardAvargaeResult> AlbumsSongsBillboardAvaragePlace(string band)
        {
            var q = from song in this.songRepo.GetAll()
                    where song.Album.Band.Name == band
                    group song by song.Album.Title into g
                    select new BillboardAvargaeResult()
                    {
                        AlbumTitle = g.Key,
                        AvaragePlace = g.Average(x => x.Billboard) ?? 0,
                    };
            return q.ToList();
        }

        /// <inheritdoc/>
        public IList<SongsWithVideos> ContainsMusicVideo(string album)
        {
            var q = from song in this.songRepo.GetAll()
                    where song.Album.Title == album
                    group song by song.MusicVideo into g
                    select new SongsWithVideos()
                    {
                        ContainsVideo = g.Key,
                        Count = g.Count(),
                    };

            return q.ToList();
        }

        /// <inheritdoc/>
        public Task<IList<BandsOriginAlbumLength>> BandsOriginPlacesAsync()
        {
            return Task<IList<BandsOriginAlbumLength>>.Factory.StartNew(() => this.BandsOriginPlaces());
        }

        /// <inheritdoc/>
        public Task<IList<BillboardAvargaeResult>> AlbumsSongBillboardAvaragePlaceAsync(string band)
        {
            return Task<IList<BillboardAvargaeResult>>.Factory.StartNew(() => this.AlbumsSongsBillboardAvaragePlace(band));
        }

        /// <inheritdoc/>
        public Task<IList<SongsWithVideos>> ContainsMusicVideoAsync(string album)
        {
            return Task<IList<SongsWithVideos>>.Factory.StartNew(() => this.ContainsMusicVideo(album));
        }
    }
}
