﻿// <copyright file="EditDataStackTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FelevesFeladat.Data;
    using FelevesFeladat.Repository;
    using Moq;
    using NUnit.Framework;

    // 3 non crud
    // 5 crud(insert, remove, update, getAll, GetOne)

    /// <summary>
    /// Tester class for the EditDataStack class.
    /// </summary>
    [TestFixture]
    public class EditDataStackTests
    {
        private Mock<IBandRepository> mockedBandRepo;
        private Mock<IAlbumRepository> mockedAlbumRepo;
        private Mock<ISongRepository> mockedSongRepo;

        /// <summary>
        /// Tests the InsertAlbum method.
        /// </summary>
        [Test]
        public void TestAlbumInsert()
        {
            var logic = this.CreateLogicWithMOcks();

            this.mockedAlbumRepo.Setup(repo => repo.Insert(It.IsAny<Album>()));

            logic.InsertAlbum("Stand up and Scream", "09.09.2009", "English", "51.09", "Sumerian", "3");

            this.mockedAlbumRepo.Verify(repo => repo.Insert(It.IsAny<Album>()), Times.Once);
        }

        /// <summary>
        /// Test the RemoveSong method.
        /// </summary>
        [Test]
        public void TestSongRemove()
        {
            var logic = this.CreateLogicWithMOcks();

            this.mockedSongRepo.Setup(repo => repo.Remove(It.IsAny<Song>()));

            logic.RemoveSong("1");

            this.mockedSongRepo.Verify(repo => repo.Remove(It.IsAny<Song>()), Times.Once);
        }

        private EditDataStackLogic CreateLogicWithMOcks()
        {
            this.mockedBandRepo = new Mock<IBandRepository>();
            this.mockedAlbumRepo = new Mock<IAlbumRepository>();
            this.mockedSongRepo = new Mock<ISongRepository>();

            List<Band> bands = new List<Band>()
            {
                new Band() { Name = "Metallica", ID = 1, Origin = "USA" },
                new Band() { Name = "Avenged Sevenfold", ID = 2, Origin = "USA" },
                new Band() { Name = "Rammstein", ID = 3, Origin = "Germany" },
            };

            List<Album> albums = new List<Album>()
            {
                new Album() { Title = "Rosenrot", BandID = bands[0].ID, Band = bands[0], ID = 1, Length = 40.01 },
                new Album() { Title = "Reise, Reise", BandID = bands[1].ID, Band = bands[1], ID = 2, Length = 65.31 },
                new Album() { Title = "Stand up and Scream", BandID = bands[2].ID, Band = bands[2], ID = 3, Length = 100.41 },
            };

            List<Song> songs = new List<Song>()
            {
                new Song() { Name = "Ohne dich", ID = 1, AlbumID = albums[0].ID, Album = albums[0], Billboard = 62, MusicVideo = true },
                new Song() { Name = "Rammlied", ID = 2, AlbumID = albums[0].ID, Album = albums[0], Billboard = 2, MusicVideo = true },
                new Song() { Name = "Mein teil", ID = 3, AlbumID = albums[1].ID, Album = albums[1], Billboard = 10 },
                new Song() { Name = "Closure", ID = 4, AlbumID = albums[2].ID, Album = albums[2], Billboard = 7 },
            };

            this.mockedBandRepo.Setup(repo => repo.GetAll()).Returns(bands.AsQueryable());
            this.mockedAlbumRepo.Setup(repo => repo.GetAll()).Returns(albums.AsQueryable());
            this.mockedSongRepo.Setup(repo => repo.GetAll()).Returns(songs.AsQueryable());

            return new EditDataStackLogic(this.mockedBandRepo.Object, this.mockedAlbumRepo.Object, this.mockedSongRepo.Object);
        }
    }
}
