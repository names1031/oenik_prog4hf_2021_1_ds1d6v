﻿// <copyright file="GetEntityByDefinitionTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using FelevesFeladat.Data;
    using FelevesFeladat.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// The tester class for the GetEntityByDefinition class.
    /// </summary>
    [TestFixture]
    public class GetEntityByDefinitionTests
    {
        private Mock<IBandRepository> mockedBandRepo;
        private Mock<IAlbumRepository> mockedAlbumRepo;
        private Mock<ISongRepository> mockedSongRepo;
        private List<BillboardAvargaeResult> mockedBillboard;
        private List<SongsWithVideos> mockedSongsWithVideos;
        private List<BandsOriginAlbumLength> mockedBandsOriginPlaces;

        /// <summary>
        /// Tests the GetAllBands method.
        /// </summary>
        [Test]
        public void TestGetAllBands()
        {
            var logic = this.CreateLogicWithMOcks();

            var result = logic.GetAllBands();

            this.mockedBandRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.mockedBandRepo.Verify(repo => repo.GetOne(It.IsAny<int>()), Times.Never);
        }

        /// <summary>
        /// Tests the getOneAlbum method.
        /// </summary>
        /// <param name="id">The album with this id wil be returned.</param>
        [TestCase("1")]
        public void TestGetOneAlbum(string id)
        {
            var logic = this.CreateLogicWithMOcks();
            this.mockedAlbumRepo.Setup(repo => repo.GetOne(It.IsAny<int>()));
            var result = logic.GetAlbumById(id);

            this.mockedAlbumRepo.Verify(repo => repo.GetAll(), Times.Never);
            this.mockedAlbumRepo.Verify(repo => repo.GetOne(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Tests the alsumSongsBillboardAvaragePlace method.
        /// </summary>
        /// <param name="band">The selected band.</param>
        [TestCase("Metallica")]
        public void TestAlbumsSongsBillboardAvaragePlace(string band)
        {
            var logic = this.CreateLogicWithMOcks();
            var billboradPlaces = logic.AlbumsSongsBillboardAvaragePlace(band);

            Assert.That(billboradPlaces, Is.EquivalentTo(this.mockedBillboard));

            this.mockedBandRepo.Verify(repo => repo.GetAll(), Times.Never);
            this.mockedAlbumRepo.Verify(repo => repo.GetAll(), Times.Never);
            this.mockedSongRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests the ContainsMusicVideoe method.
        /// </summary>
        /// <param name="album">The selected album.</param>
        [TestCase("Rosenrot")]
        public void TestContainsMusicVideo(string album)
        {
            var logic = this.CreateLogicWithMOcks();
            var containsVideo = logic.ContainsMusicVideo(album);

            Assert.That(containsVideo, Is.EquivalentTo(this.mockedSongsWithVideos));

            this.mockedBandRepo.Verify(repo => repo.GetAll(), Times.Never);
            this.mockedAlbumRepo.Verify(repo => repo.GetAll(), Times.Never);
            this.mockedSongRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test the BandsOriginPlaces method.
        /// </summary>
        [Test]
        public void TestBandsOriginPlaces()
        {
            var logic = this.CreateLogicWithMOcks();
            var originPlaces = logic.BandsOriginPlaces();

            Assert.That(originPlaces, Is.EquivalentTo(this.mockedBandsOriginPlaces));

            this.mockedBandRepo.Verify(repo => repo.GetAll(), Times.Never);
            this.mockedAlbumRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.mockedSongRepo.Verify(repo => repo.GetAll(), Times.Never);
        }

        private GetEntityByDefinitionLogic CreateLogicWithMOcks()
        {
            this.mockedBandRepo = new Mock<IBandRepository>();
            this.mockedAlbumRepo = new Mock<IAlbumRepository>();
            this.mockedSongRepo = new Mock<ISongRepository>();

            List<Band> bands = new List<Band>()
            {
                new Band() { Name = "Metallica", ID = 1, Origin = "USA" },
                new Band() { Name = "Avenged Sevenfold", ID = 2, Origin = "USA" },
                new Band() { Name = "Rammstein", ID = 3, Origin = "Germany" },
            };

            List<Album> albums = new List<Album>()
            {
                new Album() { Title = "Rosenrot", BandID = bands[0].ID, Band = bands[0], ID = 1, Length = 40.01 },
                new Album() { Title = "Reise, Reise", BandID = bands[1].ID, Band = bands[1], ID = 2, Length = 65.31 },
                new Album() { Title = "Stand up and Scream", BandID = bands[2].ID, Band = bands[2], ID = 3, Length = 100.41 },
            };

            List<Song> songs = new List<Song>()
            {
                new Song() { Name = "Ohne dich", ID = 1, AlbumID = albums[0].ID, Album = albums[0], Billboard = 62, MusicVideo = true },
                new Song() { Name = "Rammlied", ID = 2, AlbumID = albums[0].ID, Album = albums[0], Billboard = 2, MusicVideo = true },
                new Song() { Name = "Mein teil", ID = 3, AlbumID = albums[1].ID, Album = albums[1], Billboard = 10 },
                new Song() { Name = "Closure", ID = 4, AlbumID = albums[2].ID, Album = albums[2], Billboard = 7 },
            };

            this.mockedBillboard = new List<BillboardAvargaeResult>()
            {
                new BillboardAvargaeResult() { AlbumTitle = "Rosenrot", AvaragePlace = 32 },
            };

            this.mockedSongsWithVideos = new List<SongsWithVideos>()
            {
                new SongsWithVideos() { ContainsVideo = true, Count = 2 },
            };

            this.mockedBandsOriginPlaces = new List<BandsOriginAlbumLength>()
            {
                new BandsOriginAlbumLength() { BandsOrigin = "USA", SumAlbumLength = 105.32 },
                new BandsOriginAlbumLength() { BandsOrigin = "Germany", SumAlbumLength = 100.41 },
            };

            this.mockedBandRepo.Setup(repo => repo.GetAll()).Returns(bands.AsQueryable());
            this.mockedAlbumRepo.Setup(repo => repo.GetAll()).Returns(albums.AsQueryable());
            this.mockedSongRepo.Setup(repo => repo.GetAll()).Returns(songs.AsQueryable());

            return new GetEntityByDefinitionLogic(this.mockedBandRepo.Object, this.mockedAlbumRepo.Object, this.mockedSongRepo.Object);
        }
    }
}
