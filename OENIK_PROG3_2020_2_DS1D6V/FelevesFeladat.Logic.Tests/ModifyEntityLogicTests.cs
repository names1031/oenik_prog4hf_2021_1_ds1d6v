﻿// <copyright file="ModifyEntityLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FelevesFeladat.Data;
    using FelevesFeladat.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// The tester class for the ModifyEntityLogic class.
    /// </summary>
    public class ModifyEntityLogicTests
    {
        private Mock<IBandRepository> mockedBandRepo;
        private Mock<IAlbumRepository> mockedAlbumRepo;
        private Mock<ISongRepository> mockedSongRepo;

        /// <summary>
        /// Tests the ChangeBandName method.
        /// </summary>
        [Test]
        public void TestUpdateBand()
        {
            var logic = this.CreateLogicWithMOcks();

            this.mockedBandRepo.Setup(repo => repo.UpdateBand(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));

            logic.UpdateBand("1", It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>());

            this.mockedBandRepo.Verify(repo => repo.UpdateBand(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        private ModifyEntityLogic CreateLogicWithMOcks()
        {
            this.mockedBandRepo = new Mock<IBandRepository>();
            this.mockedAlbumRepo = new Mock<IAlbumRepository>();
            this.mockedSongRepo = new Mock<ISongRepository>();

            List<Band> bands = new List<Band>()
            {
                new Band() { Name = "Metallica", ID = 1, Origin = "USA" },
                new Band() { Name = "Avenged Sevenfold", ID = 2, Origin = "USA" },
                new Band() { Name = "Rammstein", ID = 3, Origin = "Germany" },
            };

            List<Album> albums = new List<Album>()
            {
                new Album() { Title = "Rosenrot", BandID = bands[0].ID, Band = bands[0], ID = 1, Length = 40.01 },
                new Album() { Title = "Reise, Reise", BandID = bands[1].ID, Band = bands[1], ID = 2, Length = 65.31 },
                new Album() { Title = "Stand up and Scream", BandID = bands[2].ID, Band = bands[2], ID = 3, Length = 100.41 },
            };

            List<Song> songs = new List<Song>()
            {
                new Song() { Name = "Ohne dich", ID = 1, AlbumID = albums[0].ID, Album = albums[0], Billboard = 62, MusicVideo = true },
                new Song() { Name = "Rammlied", ID = 2, AlbumID = albums[0].ID, Album = albums[0], Billboard = 2, MusicVideo = true },
                new Song() { Name = "Mein teil", ID = 3, AlbumID = albums[1].ID, Album = albums[1], Billboard = 10 },
                new Song() { Name = "Closure", ID = 4, AlbumID = albums[2].ID, Album = albums[2], Billboard = 7 },
            };

            this.mockedBandRepo.Setup(repo => repo.GetAll()).Returns(bands.AsQueryable());
            this.mockedAlbumRepo.Setup(repo => repo.GetAll()).Returns(albums.AsQueryable());
            this.mockedSongRepo.Setup(repo => repo.GetAll()).Returns(songs.AsQueryable());

            return new ModifyEntityLogic(this.mockedBandRepo.Object, this.mockedAlbumRepo.Object, this.mockedSongRepo.Object);
        }
    }
}
