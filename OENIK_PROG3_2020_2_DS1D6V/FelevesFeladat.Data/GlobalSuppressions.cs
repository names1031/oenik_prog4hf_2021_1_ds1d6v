﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "It makes the code unnecessarily chaotic.", Scope = "member", Target = "~M:FelevesFeladat.Data.BandContext.OnConfiguring(Microsoft.EntityFrameworkCore.DbContextOptionsBuilder)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "It makes the code unnecessarily chaotic.", Scope = "member", Target = "~M:FelevesFeladat.Data.BandContext.OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1014:MarkAssembliesWithClsCompliant", Justification = "Late bound")]
