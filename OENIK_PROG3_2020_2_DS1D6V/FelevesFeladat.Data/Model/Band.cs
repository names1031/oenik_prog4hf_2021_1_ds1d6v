﻿// <copyright file="Band.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Band class.
    /// </summary>
    [Table("Bands")]
    public class Band
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Band"/> class.
        /// Initializes the list of the band's albums.
        /// </summary>
        public Band()
        {
            this.Albums = new HashSet<Album>();
        }

        /// <summary>
        /// Gets or sets the primary key of the band class.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets a band's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets where the band was founded.
        /// </summary>
        public string Origin { get; set; }

        /// <summary>
        /// Gets or sets the members of the band.
        /// </summary>
        public string Members { get; set; }

        /// <summary>
        /// Gets or sets the bands genres.
        /// </summary>
        public string Genres { get; set; }

        /// <summary>
        /// Gets or sets when the band was formed.
        /// </summary>
        public DateTime Formed { get; set; }

        /// <summary>
        /// Gets the collection of the album.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Album> Albums { get; }

        /// <summary>
        /// Overrides the Equals method for the class.
        /// </summary>
        /// <param name="obj">The other obj that the comparison is based on.</param>
        /// <returns>If this object is equal to the other object or not.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Band)
            {
                Band otherBand = obj as Band;

                // return this.Albums == otherBand.Albums && this.Formed == otherBand.Formed && this.Genres == otherBand.Genres && this.ID == otherBand.ID && this.Members == otherBand.Members && this.Name == otherBand.Name && this.Origin == otherBand.Origin;
                return this.Name == otherBand.Name && this.ID == otherBand.ID;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Overrides the GetHashCode method for the class.
        /// </summary>
        /// <returns>The hashcode of the class.</returns>
        public override int GetHashCode()
        {
            return this.ID;
        }

        /// <summary>
        /// Overrides the to string method.
        /// </summary>
        /// <returns>The string value.</returns>
        public override string ToString()
        {
            return $"{this.Name}";
        }
    }
}
