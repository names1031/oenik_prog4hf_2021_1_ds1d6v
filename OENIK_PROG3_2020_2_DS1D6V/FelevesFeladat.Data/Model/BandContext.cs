﻿// <copyright file="BandContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Data
{
    using System;
    using System.Globalization;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// BandContext class that contains the specified datas.
    /// </summary>
    public class BandContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BandContext"/> class.
        /// Initializes the database.
        /// </summary>
        public BandContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets all the bands in a list.
        /// </summary>
        public virtual DbSet<Band> Bands { get; set; }

        /// <summary>
        /// Gets or sets all the albums in a list.
        /// </summary>
        public virtual DbSet<Album> Albums { get; set; }

        /// <summary>
        /// Gets or sets all the songs in a list.
        /// </summary>
        public virtual DbSet<Song> Songs { get; set; }

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseLazyLoadingProxies().UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Database.mdf;Integrated Security=True");
            }
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Album>(entity =>
            {
                entity.HasOne(album => album.Band)
                .WithMany(band => band.Albums)
                .HasForeignKey(album => album.BandID)
                .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Song>(entity =>
            {
                entity.HasOne(song => song.Album)
                .WithMany(album => album.Songs)
                .HasForeignKey(song => song.AlbumID)
                .OnDelete(DeleteBehavior.Cascade);
            });

            Band gunsNRoses = new Band() { ID = 1, Name = "Guns n' Roses", Origin = "USA", Members = "Axl Rose, Slash, Duff McKagan, Dizzy Reed, Richard Fortus", Genres = "Hard rock, heavy metal", Formed = DateTime.Parse("06.13.1985", new CultureInfo("en")) };
            Band metallica = new Band() { ID = 2, Name = "Metallica", Origin = "USA", Members = "James Hetfield, Lars Ulrich, Kirk Hammett, Robert Trujillo", Genres = "Trash metal, heavy metal, speed metal, hard rock", Formed = DateTime.Parse("10.28.1982", new CultureInfo("en")) };
            Band avengedSevenfold = new Band() { ID = 3, Name = "Avenged Sevenfold", Origin = "USA", Members = "M. Shadows, Synyster Gates, Zacky Vengeance, Johnny Christ, Brooks Wackerman", Genres = "Heavy metal, metalcore, progressive metal, hard rock", Formed = DateTime.Parse("09.21.1999", new CultureInfo("en")) };
            Band rammstein = new Band() { ID = 4, Name = "Rammstein", Origin = "Germany", Members = "Till Lindemann, Richard Z. Kruspe, Paul H. Landers, Oliver Riedel, Christop Schneider, Christian Lorenz", Genres = "Industrial metal, gothic metal", Formed = DateTime.Parse("01.07.1994", new CultureInfo("en")) };

            Album appetiteForDestruction = new Album() { ID = 1, BandID = gunsNRoses.ID, Title = "Appetite for Destruction", Released = DateTime.Parse("07.21.1987", new CultureInfo("en")), Language = "English", Length = 53.50, Label = "Geffren" };
            Album useYourIllusion = new Album() { ID = 2, BandID = gunsNRoses.ID, Title = "Use Your Illusion I", Released = DateTime.Parse("09.17.1991", new CultureInfo("en")), Language = "English", Length = 76.09, Label = "Geffren" };
            Album chineseDemocracy = new Album() { ID = 3, BandID = gunsNRoses.ID, Title = "Chinese Democracy", Released = DateTime.Parse("11.23.2008", new CultureInfo("en")), Language = "English", Length = 71.18, Label = "Geffren" };

            Album masterOfPuppets = new Album() { ID = 4, BandID = metallica.ID, Title = "Master of Puppets", Released = DateTime.Parse("03.03.1986", new CultureInfo("en")), Language = "English", Length = 54.47, Label = "Elektra" };
            Album andJusticeForAll = new Album() { ID = 5, BandID = metallica.ID, Title = "...And Justice for All", Released = DateTime.Parse("09.07.1988", new CultureInfo("en")), Language = "English", Length = 65.24, Label = "Elektra" };
            Album metallicaSelfTitle = new Album() { ID = 6, BandID = metallica.ID, Title = "Metallica", Released = DateTime.Parse("08.12.1991", new CultureInfo("en")), Language = "English", Length = 62.40, Label = "Elektra" };

            Album cityOfEvil = new Album() { ID = 7, BandID = avengedSevenfold.ID, Title = "City of Evil", Released = DateTime.Parse("06.06.2005", new CultureInfo("en")), Language = "English", Length = 72.43, Label = "Warner Bros." };
            Album avengedSevenfoldSelfTitle = new Album() { ID = 8, BandID = avengedSevenfold.ID, Title = "Avenged Sevenfold", Released = DateTime.Parse("10.30.2007", new CultureInfo("en")), Language = "English", Length = 53.07, Label = "Warner Bros." };
            Album nightmare = new Album() { ID = 9, BandID = avengedSevenfold.ID, Title = "Nightmare", Released = DateTime.Parse("07.27.2010", new CultureInfo("en")), Language = "English", Length = 66.46, Label = "Warner Bros." };

            Album mutter = new Album() { ID = 10, BandID = rammstein.ID, Title = "Mutter", Released = DateTime.Parse("04.02.2001", new CultureInfo("en")), Language = "German", Length = 45.02, Label = "Universal" };
            Album reiseReise = new Album() { ID = 11, BandID = rammstein.ID, Title = "Reise, Reise", Released = DateTime.Parse("09.27.2004", new CultureInfo("en")), Language = "German", Length = 47.45, Label = "Universal" };
            Album rosenrot = new Album() { ID = 12, BandID = rammstein.ID, Title = "Rosenrot", Released = DateTime.Parse("10.28.2005", new CultureInfo("en")), Language = "German", Length = 48.00, Label = "Universal" };

            Song welcometoTheJungle = new Song() { ID = 1, AlbumID = appetiteForDestruction.ID, Name = "Welcome to the Jungle", MusicVideo = true, Length = 4.31, GuitarSolo = true, Billboard = 7 };
            Song nightTrain = new Song() { ID = 2, AlbumID = appetiteForDestruction.ID, Name = "NightTrain", MusicVideo = false, Length = 3.21, GuitarSolo = true, Billboard = 93 };
            Song sweetChildOfMine = new Song() { ID = 3, AlbumID = appetiteForDestruction.ID, Name = "Sweet Child o' Mine", MusicVideo = true, Length = 5.55, GuitarSolo = true, Billboard = 5 };
            Song paradiseCity = new Song() { ID = 4, AlbumID = appetiteForDestruction.ID, Name = "Paradise City", MusicVideo = true, Length = 6.46, GuitarSolo = true, Billboard = 5 };

            Song liveAndLetDie = new Song() { ID = 5, AlbumID = useYourIllusion.ID, Name = "Live and Let Die", MusicVideo = false, Length = 3.04, GuitarSolo = true, Billboard = 20 };
            Song dontCry = new Song() { ID = 6, AlbumID = useYourIllusion.ID, Name = "Dont' Cry", MusicVideo = true, Length = 4.45, GuitarSolo = true, Billboard = 10 };
            Song novemberRain = new Song() { ID = 7, AlbumID = useYourIllusion.ID, Name = "November Rain", MusicVideo = true, Length = 8.57, GuitarSolo = true, Billboard = 3 };
            Song theGarden = new Song() { ID = 8, AlbumID = useYourIllusion.ID, Name = "The Garden", MusicVideo = true, Length = 5.21, GuitarSolo = true, Billboard = null };

            Song chineseDemocracySong = new Song() { ID = 9, AlbumID = chineseDemocracy.ID, Name = "Chinese Democracy", MusicVideo = false, Length = 4.43, GuitarSolo = true, Billboard = null };

            Song battery = new Song() { ID = 10, AlbumID = masterOfPuppets.ID, Name = "Battery", MusicVideo = false, Length = 5.13, GuitarSolo = true, Billboard = null };
            Song masterOfPuppetsSong = new Song() { ID = 11, AlbumID = masterOfPuppets.ID, Name = "Master of Puppets", MusicVideo = false, Length = 8.36, GuitarSolo = true, Billboard = 22 };
            Song welcomeHome = new Song() { ID = 12, AlbumID = masterOfPuppets.ID, Name = "Welcome Home (Sanitarium)", MusicVideo = false, Length = 6.27, GuitarSolo = true, Billboard = null };
            Song orion = new Song() { ID = 13, AlbumID = masterOfPuppets.ID, Name = "Orion", MusicVideo = false, Length = 8.27, GuitarSolo = true, Billboard = null };

            Song blackened = new Song() { ID = 14, AlbumID = andJusticeForAll.ID, Name = "Blackened", MusicVideo = false, Length = 6.41, GuitarSolo = true, Billboard = null };
            Song andJusticeForAllSong = new Song() { ID = 15, AlbumID = andJusticeForAll.ID, Name = "...And Justice for All", MusicVideo = false, Length = 9.47, GuitarSolo = true, Billboard = null };
            Song one = new Song() { ID = 16, AlbumID = andJusticeForAll.ID, Name = "One", MusicVideo = true, Length = 7.27, GuitarSolo = true, Billboard = 35 };

            Song enterSandman = new Song() { ID = 17, AlbumID = metallicaSelfTitle.ID, Name = "Enter Sandman", MusicVideo = true, Length = 5.34, GuitarSolo = true, Billboard = 16 };
            Song sadButTrue = new Song() { ID = 18, AlbumID = metallicaSelfTitle.ID, Name = "Sad but True", MusicVideo = false, Length = 5.24, GuitarSolo = true, Billboard = 98 };
            Song theUnforgiven = new Song() { ID = 19, AlbumID = metallicaSelfTitle.ID, Name = "The Unforgiven", MusicVideo = true, Length = 6.26, GuitarSolo = true, Billboard = 35 };
            Song nothingElseMatters = new Song() { ID = 20, AlbumID = metallicaSelfTitle.ID, Name = "Nothing Else Matters", MusicVideo = true, Length = 6.30, GuitarSolo = true, Billboard = 11 };
            Song theGodThatFailed = new Song() { ID = 21, AlbumID = metallicaSelfTitle.ID, Name = "The God That Failed", MusicVideo = false, Length = 5.09, GuitarSolo = true, Billboard = null };

            Song beastAndTheHarlot = new Song() { ID = 22, AlbumID = cityOfEvil.ID, Name = "Beast and the Harlot", MusicVideo = true, Length = 5.42, GuitarSolo = true, Billboard = null };
            Song batCountry = new Song() { ID = 23, AlbumID = cityOfEvil.ID, Name = "Bat Country", MusicVideo = true, Length = 5.13, GuitarSolo = true, Billboard = 91 };
            Song burnItDown = new Song() { ID = 24, AlbumID = cityOfEvil.ID, Name = "Burn It Down", MusicVideo = true, Length = 5.00, GuitarSolo = true, Billboard = null };
            Song seizeTheDay = new Song() { ID = 25, AlbumID = cityOfEvil.ID, Name = "Seize the Day", MusicVideo = true, Length = 5.35, GuitarSolo = true, Billboard = null };

            Song almostEasy = new Song() { ID = 26, AlbumID = avengedSevenfoldSelfTitle.ID, Name = "Almost Easy", MusicVideo = true, Length = 3.55, GuitarSolo = true, Billboard = null };
            Song scream = new Song() { ID = 27, AlbumID = avengedSevenfoldSelfTitle.ID, Name = "Scream", MusicVideo = false, Length = 4.49, GuitarSolo = true, Billboard = null };
            Song afterlife = new Song() { ID = 28, AlbumID = avengedSevenfoldSelfTitle.ID, Name = "Afterlife", MusicVideo = true, Length = 5.53, GuitarSolo = true, Billboard = 20 };
            Song aLittlePieceOfHeaven = new Song() { ID = 29, AlbumID = avengedSevenfoldSelfTitle.ID, Name = "A Little Piece of Heaven", MusicVideo = true, Length = 8.01, GuitarSolo = true, Billboard = null };

            Song nightmareSong = new Song() { ID = 30, AlbumID = nightmare.ID, Name = "Nightmare", MusicVideo = true, Length = 6.15, GuitarSolo = true, Billboard = 8 };
            Song welcomeToTheFamily = new Song() { ID = 31, AlbumID = nightmare.ID, Name = "Welcome to the Family", MusicVideo = false, Length = 4.05, GuitarSolo = true, Billboard = 23 };
            Song buriedAlive = new Song() { ID = 32, AlbumID = nightmare.ID, Name = "Buried Alive", MusicVideo = true, Length = 6.44, GuitarSolo = true, Billboard = null };
            Song soFarAway = new Song() { ID = 33, AlbumID = nightmare.ID, Name = "So Far Away", MusicVideo = true, Length = 5.26, GuitarSolo = true, Billboard = 15 };

            Song meinHerzBrennt = new Song() { ID = 34, AlbumID = mutter.ID, Name = "Mein Herz brennt", MusicVideo = true, Length = 4.39, GuitarSolo = false, Billboard = null };
            Song sonne = new Song() { ID = 35, AlbumID = mutter.ID, Name = "Sonne", MusicVideo = true, Length = 4.32, GuitarSolo = false, Billboard = null };
            Song ichWill = new Song() { ID = 36, AlbumID = mutter.ID, Name = "Ich Will", MusicVideo = true, Length = 3.37, GuitarSolo = false, Billboard = null };
            Song mutterSong = new Song() { ID = 37, AlbumID = mutter.ID, Name = "Mutter", MusicVideo = true, Length = 4.32, GuitarSolo = false, Billboard = null };

            Song reiseReiseSong = new Song() { ID = 38, AlbumID = reiseReise.ID, Name = "Reise, Reise", MusicVideo = false, Length = 4.11, GuitarSolo = false, Billboard = null };
            Song meinTeil = new Song() { ID = 39, AlbumID = reiseReise.ID, Name = "Mein Teil", MusicVideo = true, Length = 4.32, GuitarSolo = false, Billboard = null };
            Song keineLust = new Song() { ID = 40, AlbumID = reiseReise.ID, Name = "Keine Lust", MusicVideo = true, Length = 3.43, GuitarSolo = false, Billboard = null };

            Song mannGegenMann = new Song() { ID = 41, AlbumID = rosenrot.ID, Name = "Mann gegen Mann", MusicVideo = true, Length = 3.50, GuitarSolo = false, Billboard = null };
            Song zerstören = new Song() { ID = 42, AlbumID = rosenrot.ID, Name = "Zerstören", MusicVideo = true, Length = 5.28, GuitarSolo = false, Billboard = null };

            modelBuilder.Entity<Band>().HasData(gunsNRoses, metallica, avengedSevenfold, rammstein);
            modelBuilder.Entity<Album>().HasData(appetiteForDestruction, useYourIllusion, chineseDemocracy, masterOfPuppets, andJusticeForAll, metallicaSelfTitle, cityOfEvil, avengedSevenfoldSelfTitle, nightmare, mutter, reiseReise, rosenrot);
            modelBuilder.Entity<Song>().HasData(welcometoTheJungle, nightTrain, sweetChildOfMine, paradiseCity, liveAndLetDie, dontCry, novemberRain, theGarden, chineseDemocracySong, battery, masterOfPuppetsSong, welcomeHome, orion, blackened, andJusticeForAllSong, one, enterSandman, sadButTrue, theUnforgiven, nothingElseMatters, theGodThatFailed, beastAndTheHarlot, batCountry, burnItDown, seizeTheDay, almostEasy, scream, afterlife, aLittlePieceOfHeaven, nightmareSong, welcomeToTheFamily, buriedAlive, soFarAway, meinHerzBrennt, sonne, ichWill, mutterSong, reiseReiseSong, meinTeil, keineLust, mannGegenMann, zerstören);
        }
    }
}
