﻿// <copyright file="Song.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Song class.
    /// </summary>
    [Table("Songs")]
    public class Song
    {
        /// <summary>
        /// Gets or sets the primary key of the songs class.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the name of a song.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a song has music video or not.
        /// </summary>
        public bool MusicVideo { get; set; }

        /// <summary>
        /// Gets or sets the length of a song.
        /// </summary>
        public double Length { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the song has a guitar solo in it or not.
        /// </summary>
        public bool GuitarSolo { get; set; }

        /// <summary>
        /// Gets or sets the best place a song reached on the USA top 100 billboard. If the song didnt reach the top 100, the property value is null.
        /// </summary>
        public int? Billboard { get; set; }

        /// <summary>
        /// Gets or sets a foreign key that references the album class.
        /// </summary>
        [ForeignKey(nameof(Album))]
        public int AlbumID { get; set; }

        /// <summary>
        /// Gets or sets initializes a new instance of the <see cref="Song"/> class.
        /// </summary>
        [NotMapped]
        public virtual Album Album { get; set; }

        /// <summary>
        /// Overrides the Equals method for the class.
        /// </summary>
        /// <param name="obj">The other obj that the comparison is based on.</param>
        /// <returns>If this object is equal to the other object or not.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Song)
            {
                Song otherSong = obj as Song;
                return this.Album == otherSong.Album && this.AlbumID == otherSong.AlbumID && this.Billboard == otherSong.Billboard && this.GuitarSolo == otherSong.GuitarSolo && this.ID == otherSong.ID && this.Length == otherSong.Length && this.MusicVideo == otherSong.MusicVideo && this.Name == otherSong.Name;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Overrides the GetHashCode method for the class.
        /// </summary>
        /// <returns>The hashcode of the class.</returns>
        public override int GetHashCode()
        {
            return this.ID;
        }

        /// <summary>
        /// Overrides the to string method.
        /// </summary>
        /// <returns>The string value.</returns>
        public override string ToString()
        {
            return $"{this.Name}";
        }
    }
}
