﻿// <copyright file="Album.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FelevesFeladat.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Album data class.
    /// </summary>
    [Table("Albums")]
    public class Album
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Album"/> class.
        /// Initializes the list of the album's songs.
        /// </summary>
        public Album()
        {
            this.Songs = new HashSet<Song>();
        }

        /// <summary>
        /// Gets or sets the primary key of the album class.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the title of an album.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets when an album was released.
        /// </summary>
        public DateTime Released { get; set; }

        /// <summary>
        /// Gets or sets the language of an album.
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the length of an album.
        /// </summary>
        public double Length { get; set; }

        /// <summary>
        /// Gets or sets the labels name.
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Gets or sets a foreign key that references the band.
        /// </summary>
        [ForeignKey(nameof(Band))]
        public int BandID { get; set; }

        /// <summary>
        /// Gets the album's songs in a list.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Song> Songs { get; }

        /// <summary>
        /// Gets or sets the reference to the band which the album belongs.
        /// </summary>
        [NotMapped]
        public virtual Band Band { get; set; }

        /// <summary>
        /// Overrides the Equals method for the class.
        /// </summary>
        /// <param name="obj">The other obj that the comparison is based on.</param>
        /// <returns>If this object is equal to the other object or not.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Album)
            {
                Album otherAlbum = obj as Album;
                return this.Band == otherAlbum.Band && this.BandID == otherAlbum.BandID && this.ID == otherAlbum.ID && this.Label == otherAlbum.Label && this.Language == otherAlbum.Language && this.Length == otherAlbum.Length && this.Released == otherAlbum.Released && this.Songs == otherAlbum.Songs && this.Title == otherAlbum.Title;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Overrides the GetHashCode method for the class.
        /// </summary>
        /// <returns>The hashcode of the class.</returns>
        public override int GetHashCode()
        {
            return this.ID;
        }

        /// <summary>
        /// Overrides the to string method.
        /// </summary>
        /// <returns>The string value.</returns>
        public override string ToString()
        {
            return $"{this.Title}";
        }
    }
}
